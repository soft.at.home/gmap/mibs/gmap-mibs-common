/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
//#include <amxp/amxp_expression.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_expression.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <arpa/inet.h>
#include <gmap/gmap.h>

/**********************************************************
* Macro definitions
**********************************************************/
#define ME "gmap_mibs"
#define MIB_INFO_MAX_PRIORITY 100
#define MIB_NAME "information"

/**********************************************************
* Type definitions
**********************************************************/


/**********************************************************
* Variable declarations
**********************************************************/

amxb_bus_ctx_t* gmap_bus = NULL;

/**********************************************************
* Function Prototypes
**********************************************************/
amxd_status_t _setInformation(amxd_object_t* object,
                              amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret);

amxd_status_t _setInformationList(amxd_object_t* object,
                                  amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret);

amxd_status_t _removeInformation(amxd_object_t* object,
                                 amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);

/**********************************************************
* Functions
**********************************************************/

static void local_gmap_client_init(UNUSED const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    if(gmap_bus == NULL) {
        gmap_bus = amxb_be_who_has("Devices.Device");
        gmap_client_init(gmap_bus);
    }
}

CONSTRUCTOR static void mib_start(void) {
    amxp_sigmngr_deferred_call(NULL, local_gmap_client_init, NULL, NULL);
}

static void get_default_parameter_value(amxd_object_t* object,
                                        const char* param_name,
                                        amxc_var_t* value) {
    when_null(value, exit);

    amxd_dm_t* dm = amxd_object_get_dm(object);
    amxd_object_t* mib = amxd_dm_get_mib(dm, MIB_NAME);
    amxd_param_t* param = amxd_object_get_param_def(mib, param_name);
    amxd_status_t status = amxd_param_get_value(param, value);

    when_true(status == amxd_status_ok, exit);

    SAH_TRACEZ_ERROR(ME, "Failed to fetch default value for %s", param_name);
    amxc_var_set(cstring_t, value, "");

exit:
    return;
}

static amxd_status_t update_information(amxd_object_t* object, amxd_object_t* type_collection, const char* type_name) {
    uint32_t priority = MIB_INFO_MAX_PRIORITY + 1;
    amxd_status_t status = amxd_status_ok;
    amxc_var_t var_name;
    amxd_trans_t trans;

    amxc_var_init(&var_name);
    amxd_trans_init(&trans);

    amxd_object_for_each(instance, it, type_collection) {
        amxd_object_t* inst = amxc_llist_it_get_data(it, amxd_object_t, it);
        const char* current_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(inst, "Value"));
        uint32_t current_priority = amxd_object_get_value(uint32_t, inst, "Priority", NULL);

        if(current_priority < priority) {
            priority = current_priority;
            amxc_var_set(cstring_t, &var_name, current_name);
        }
    }

    if(priority == (MIB_INFO_MAX_PRIORITY + 1)) {
        SAH_TRACEZ_INFO(ME, "No sources remaining, emptying %s", type_name);
        get_default_parameter_value(object, type_name, &var_name);
    }

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_param(&trans, type_name, &var_name);
    status = amxd_trans_apply(&trans, gmap_client_get_mib_server_dm());

    amxd_trans_clean(&trans);
    amxc_var_clean(&var_name);

    return status;
}

/**
 * @brief
 * Fills the given type_string with the correct object name for the given type name.
 *
 * @param type_string Pointer to the string object that will be filled.
 *                    This pointer must not be `NULL`.
 *                    This pointer must point to an initialized string object.
 * @param type_name Type name.
 */
static void type_name_to_type_string(amxc_string_t* type_string, const char* type_name) {
    when_null(type_string, exit);
    when_str_empty(type_name, exit);

    amxc_string_set(type_string, type_name);
    if((strcmp(amxc_string_get(type_string, amxc_string_text_length(type_string) - 1), "y") == 0 )) {
        amxc_string_shrink(type_string, 1);
        amxc_string_appendf(type_string, "ies");
    } else {
        amxc_string_appendf(type_string, "s");
    }

exit:
    return;
}

amxd_status_t _setInformation(amxd_object_t* object,
                              UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxp_expr_status_t expr_status = amxp_expr_status_unknown_error;
    const uint32_t priority = GET_UINT32(args, "priority");
    const char* source = GET_CHAR(args, "source");
    const char* type_name = GET_CHAR(args, "type");
    const char* value = GET_CHAR(args, "value");
    const char* type = NULL;
    amxc_string_t type_string;
    amxp_expr_t expr;
    amxd_object_t* type_collection = NULL;
    amxd_object_t* info_obj = NULL;
    amxd_trans_t trans;

    amxc_string_init(&type_string, 0);
    amxd_trans_init(&trans);

    amxc_var_set(bool, ret, false);
    expr_status = amxp_expr_buildf(&expr, "Source == '%s'", source);
    when_failed_trace(expr_status, exit, ERROR, "Build expression for source %s failed: %d", source, expr_status);

    /* Check if the arguments are correct */
    status = amxd_status_missing_key;
    when_str_empty(type_name, exit);
    when_str_empty(value, exit);
    when_str_empty(source, exit);

    status = amxd_status_invalid_arg;
    when_false_trace(((0 != priority) && (priority <= MIB_INFO_MAX_PRIORITY)), exit, ERROR, "Priority should be between 1 and %d", MIB_INFO_MAX_PRIORITY);

    /* check if the type is one of the existing information types */
    type_name_to_type_string(&type_string, type_name);
    type = amxc_string_get(&type_string, 0);

    type_collection = amxd_object_get_child(object, type);
    status = amxd_status_object_not_found;
    when_null(type_collection, exit);

    /* Check if the instance already exists using the source value*/
    info_obj = amxd_object_find_instance(type_collection, &expr);
    if(info_obj == NULL) {
        amxd_trans_select_object(&trans, type_collection);
        /* The mod_pcm importer can't handle objects with only an index and no name,
         * so use the Source as the object name. */
        amxd_trans_add_inst(&trans, 0, source);
        amxd_trans_set_cstring_t(&trans, "Source", source);
    } else {
        amxd_trans_select_object(&trans, info_obj);
    }

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_cstring_t(&trans, "Value", value);
    amxd_trans_set_uint32_t(&trans, "Priority", priority);

    status = amxd_trans_apply(&trans, gmap_client_get_mib_server_dm());
    when_failed_trace(status, exit, ERROR, "Failed to set information %s: %d", type_name, status);

    status = update_information(object, type_collection, type_name);

    if(status == amxd_status_ok) {
        amxc_var_set(bool, ret, true);
    }

exit:
    amxp_expr_clean(&expr);
    amxc_string_clean(&type_string);
    amxd_trans_clean(&trans);
    return status;
}

amxd_status_t _setInformationList(amxd_object_t* object,
                                  amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_unknown_error;
    amxc_var_t* args_list = GET_ARG(args, "informationList");

    when_null(args, exit);
    when_null(args_list, exit);

    amxc_var_for_each(information, args_list) {
        const char* type = GET_CHAR(information, "type");
        const char* source = GET_CHAR(information, "source");
        const char* value = GET_CHAR(information, "value");

        amxc_var_t* var_prio = GET_ARG(information, "priority");

        retval = amxd_status_missing_key;
        when_str_empty(type, exit);
        when_str_empty(source, exit);
        when_str_empty(value, exit);

        if(var_prio == NULL) {
            amxc_var_add_key(uint32_t, information, "priority", 100);
        }

        retval = _setInformation(object, func, information, ret);
        when_failed_trace(retval, exit, ERROR, "ERROR: Could not set information value for type: %s, source: %s, value: %s\n", type, source, value);
    }

    retval = amxd_status_ok;

exit:
    return retval;
}

amxd_status_t _removeInformation(amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxp_expr_status_t expr_status = amxp_expr_status_unknown_error;
    const char* source = GET_CHAR(args, "source");
    const char* type_name = GET_CHAR(args, "type");
    const char* type = NULL;
    amxc_string_t type_string;
    amxp_expr_t expr;
    amxd_object_t* type_collection = NULL;
    amxd_object_t* info_obj = NULL;
    amxd_trans_t trans;

    amxc_string_init(&type_string, 0);
    amxd_trans_init(&trans);

    amxc_var_set(bool, ret, false);
    expr_status = amxp_expr_buildf(&expr, "Source == '%s'", source);
    when_failed_trace(expr_status, exit, ERROR, "Build expression for source %s failed: %d", source, expr_status);

    /* Check if the arguments are correct */
    status = amxd_status_missing_key;
    when_str_empty(type_name, exit);
    when_str_empty(source, exit);

    /* check if the type is one of the existing information types */
    type_name_to_type_string(&type_string, type_name);
    type = amxc_string_get(&type_string, 0);

    type_collection = amxd_object_get_child(object, type);
    status = amxd_status_object_not_found;
    when_null_trace(type_collection, exit, ERROR, "Type object %s not found", type);

    /* Check if the instance exists, if it does remove it*/
    info_obj = amxd_object_find_instance(type_collection, &expr);
    when_null_trace(info_obj, exit, ERROR, "%s object with source %s not found", type, source);
    amxd_trans_select_object(&trans, type_collection);
    amxd_trans_del_inst(&trans, amxd_object_get_index(info_obj), NULL);

    status = amxd_trans_apply(&trans, gmap_client_get_mib_server_dm());
    when_failed_trace(status, exit, ERROR, "Failed to remove information %s: %d", type_name, status);

    status = update_information(object, type_collection, type_name);

    if(status == amxd_status_ok) {
        amxc_var_set(bool, ret, true);
    }

exit:
    amxp_expr_clean(&expr);
    amxc_string_clean(&type_string);
    amxd_trans_clean(&trans);
    return status;
}
