/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/* Enable GNU extensions such as strdup */
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_expression.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <gmap/gmap.h>

/**********************************************************
* Macro definitions
**********************************************************/
#define ME "gmap_mibs"
#define MIB_NAME "stats"
#define PARAM_COLLECTION_INTERVAL "CollectionInterval"
#define PARAM_ORIGIN "Origin"
#define PARAM_LAST_UPDATE "LastUpdate"
#define PARAM_INITIAL_TIME "InitialTime"
#define PARAM_STATUS "Status"
#define STATUS_INIT "Init"
#define STATUS_SUCCESS "Success"
#define STATUS_ERROR "Error"
#define ARG_ORIGIN "origin"
#define ARG_VALUES "values"


/**********************************************************
* Type definitions
**********************************************************/

typedef struct gmap_mibs_stats_device_info {
    amxc_htable_it_t it;
    char* stats_origin;      /**< Last origin used when submitting statistics. NULL if no previously used origin. Owned. */
    char* error_origin;      /**< Last origin used when setting the error state. NULL if no previously used origin. Owned. */
} gmap_mibs_stats_device_info_t;


/**********************************************************
* Variable declarations
**********************************************************/

static const char* const s_stat_names[] = {
    "BytesSent",
    "BytesReceived",
    "PacketsSent",
    "PacketsReceived",
    NULL
};

static struct {
    amxc_htable_t devices;
} s_mib_data;

/**********************************************************
* Function Prototypes
**********************************************************/
amxd_status_t _setStatistics(amxd_object_t* object,
                             amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret);

amxd_status_t _setStatisticsError(amxd_object_t* object,
                                  amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret);

void _stats_event_object_added(const char* const sig_name,
                               const amxc_var_t* const data,
                               void* const priv);
void _stats_event_object_removed(const char* const sig_name,
                                 const amxc_var_t* const data,
                                 void* const priv);
void _stats_event_object_changed_origin(const char* const sig_name,
                                        const amxc_var_t* const data,
                                        void* const priv);

/**********************************************************
* Functions
**********************************************************/

static void s_device_info_delete(UNUSED const char* key, amxc_htable_it_t* it) {
    gmap_mibs_stats_device_info_t* info = amxc_htable_it_get_data(it, gmap_mibs_stats_device_info_t, it);

    when_null(it, exit);

    free(info->stats_origin);
    free(info->error_origin);
    free(info);

exit:
    return;
}

static gmap_mibs_stats_device_info_t* s_device_info_new(const char* key) {
    int status = 0;
    gmap_mibs_stats_device_info_t* info = calloc(1, sizeof(gmap_mibs_stats_device_info_t));
    when_null_trace(info, error, ERROR, "Out of Memory");

    amxc_htable_it_init(&info->it);
    status = amxc_htable_insert(&s_mib_data.devices, key, &info->it);
    when_failed_trace(status, error, ERROR, "Out of Memory");

    return info;

error:
    if(info != NULL) {
        s_device_info_delete(key, &info->it);
    }
    return NULL;
}

static gmap_mibs_stats_device_info_t* s_device_info_for_path(const char* path) {
    gmap_mibs_stats_device_info_t* info = NULL;
    amxc_htable_it_t* it = amxc_htable_get(&s_mib_data.devices, path);

    when_null(it, exit);

    info = amxc_htable_it_get_data(it, gmap_mibs_stats_device_info_t, it);

exit:
    return info;
}

static gmap_mibs_stats_device_info_t* s_device_info_for_object(const amxd_object_t* object) {
    gmap_mibs_stats_device_info_t* info = NULL;
    char* path = NULL;

    when_null(object, exit);

    path = amxd_object_get_path(object, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    info = s_device_info_for_path(path);

exit:
    free(path);
    return info;
}

static void s_device_info_set_stats_origin(gmap_mibs_stats_device_info_t* info, const char* origin) {
    when_null(info, exit);
    bool same_origin = ((origin == info->stats_origin) ||
                        (origin != NULL && info->stats_origin != NULL &&
                         strcmp(origin, info->stats_origin) == 0));
    when_true(same_origin, exit);

    free(info->stats_origin);
    if((origin == NULL) || (*origin == '\0')) {
        info->stats_origin = NULL;
    } else if(!same_origin) {
        info->stats_origin = strdup(origin);
    }

exit:
    return;
}

static void s_device_info_set_error_origin(gmap_mibs_stats_device_info_t* info, const char* origin) {
    when_null(info, exit);
    bool same_origin = ((origin == info->error_origin) ||
                        (origin != NULL && info->error_origin != NULL &&
                         strcmp(origin, info->error_origin) == 0));
    when_true(same_origin, exit);

    free(info->error_origin);
    if((origin == NULL) || (*origin == '\0')) {
        info->error_origin = NULL;
    } else if(!same_origin) {
        info->error_origin = strdup(origin);
    }

exit:
    return;
}


CONSTRUCTOR static void mib_start(void) {
    amxc_htable_init(&s_mib_data.devices, 0);
}

DESTRUCTOR static void mib_end(void) {
    amxc_htable_clean(&s_mib_data.devices, s_device_info_delete);
}

void _stats_event_object_added(const char* const sig_name,
                               const amxc_var_t* const data,
                               UNUSED void* const priv) {
    const char* path = GET_CHAR(data, "path");
    amxc_htable_it_t* it = NULL;

    when_str_empty_trace(sig_name, exit, ERROR, "NULL/empty");
    when_null_trace(data, exit, ERROR, "NULL");
    when_false(strcmp(sig_name, "dm:object-added") == 0, exit);
    when_str_empty_trace(path, exit, ERROR, "NULL/empty");

    it = amxc_htable_get(&s_mib_data.devices, path);
    when_not_null_trace(it, exit, ERROR, "Entry for device '%s' already exists", path);
    s_device_info_new(path);

exit:
    return;
}

void _stats_event_object_removed(const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    const char* path = GET_CHAR(data, "path");
    amxc_htable_it_t* it = NULL;

    when_str_empty_trace(sig_name, exit, ERROR, "NULL/empty");
    when_null_trace(data, exit, ERROR, "NULL");
    when_false(strcmp(sig_name, "dm:object-removed") == 0, exit);
    when_str_empty_trace(path, exit, ERROR, "NULL/empty");

    it = amxc_htable_get(&s_mib_data.devices, path);
    when_null_trace(it, exit, WARNING, "Missing or double free of entry for device '%s'", path);

    amxc_htable_it_clean(it, s_device_info_delete);

exit:
    return;
}

/** Reset all automatic values to the initial state. */
static void s_reset_all_values(amxd_trans_t* trans, const char* status) {
    amxc_var_t timestamp;

    amxc_var_init(&timestamp);
    amxc_var_set_type(&timestamp, AMXC_VAR_ID_TIMESTAMP);

    for(const char* const* name_it = s_stat_names; *name_it != NULL; ++name_it) {
        const char* key = *name_it;
        amxd_trans_set_value(uint64_t, trans, key, 0);
    }

    amxd_trans_set_param(trans, PARAM_INITIAL_TIME, &timestamp);
    amxd_trans_set_param(trans, PARAM_LAST_UPDATE, &timestamp);
    amxd_trans_set_value(cstring_t, trans, PARAM_STATUS, status);

    amxc_var_clean(&timestamp);
    return;
}

/** @grief Fetches a uint64 from a variant
 *
 * Only accepts integer variants and performs a range check.
 */
static uint64_t s_get_uint64_t_safe(const amxc_var_t* var, amxd_status_t* status) {
    uint64_t out = 0;
    int64_t out_signed = 0;
    amxc_var_t converted;

    amxc_var_init(&converted);

    when_null(status, exit);
    *status = amxd_status_invalid_value;
    when_null(var, exit);

    switch(amxc_var_type_of(var)) {
    case AMXC_VAR_ID_INT8:
    case AMXC_VAR_ID_INT16:
    case AMXC_VAR_ID_INT32:
    case AMXC_VAR_ID_INT64:
        out_signed = amxc_var_dyncast(int64_t, var);
        when_false_trace(out_signed >= 0, exit, ERROR, "Negative value");
        out = (uint64_t) out_signed;
        *status = amxd_status_ok;
        break;
    case AMXC_VAR_ID_UINT8:
    case AMXC_VAR_ID_UINT16:
    case AMXC_VAR_ID_UINT32:
    case AMXC_VAR_ID_UINT64:
        out = amxc_var_dyncast(uint64_t, var);
        *status = amxd_status_ok;
        break;
    default:
        /* amxc_var_convert has a quirk where converting to unsigned takes the
         * absolute value of the conversion to signed, instead of failing on negative input. */
        when_failed_trace(amxc_var_convert(&converted, var, AMXC_VAR_ID_INT64),
                          exit, ERROR, "Not a valid number: %s (%d)",
                          amxc_var_type_name_of(var), amxc_var_type_of(var));
        out_signed = amxc_var_dyncast(int64_t, &converted);
        when_false_trace(out_signed >= 0, exit, ERROR, "Negative value");
        out = (uint64_t) out_signed;
        *status = amxd_status_ok;
        break;
    }

exit:
    amxc_var_clean(&converted);
    return out;
}

/**
 * Updates the read-only parameters for the given values.
 *
 * @param trans Update operations will be added to this transaction.
 * @param update_initial If `true`, both the initial and last timestamp will be updated.
 *                       If `false`, only the last timestamp is updated.
 */
static amxd_status_t s_update_values(amxd_trans_t* trans,
                                     const amxc_var_t* values,
                                     bool update_initial) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_ts_t timestamp = { 0 };

    when_failed_trace(amxc_ts_now(&timestamp), exit, ERROR, "Failed to get the current time.");

    // Set the values from the argument
    amxc_var_for_each(it, values) {
        const char* key = amxc_var_key(it);
        uint64_t new_value = s_get_uint64_t_safe(it, &status);
        when_failed(status, exit);
        status = amxd_trans_set_value(uint64_t, trans, key, new_value);
        when_failed(status, exit);
    }

    if(update_initial) {
        // Reset the values not set by the parameter
        for(const char* const* it = s_stat_names; *it != NULL; ++it) {
            const char* key = *it;
            if(GET_ARG(values, key) != NULL) {
                continue;
            }
            amxd_trans_set_value(uint64_t, trans, key, 0);
            when_failed(status, exit);
        }

        status = amxd_trans_set_value(amxc_ts_t, trans, PARAM_INITIAL_TIME, &timestamp);
        when_failed(status, exit);
    }
    status = amxd_trans_set_value(amxc_ts_t, trans, PARAM_LAST_UPDATE, &timestamp);
    when_failed(status, exit);
    status = amxd_trans_set_value(cstring_t, trans, PARAM_STATUS, STATUS_SUCCESS);

exit:
    return status;
}

/** @brief Performs basic values argument checking. Only types and keys are verified, not the values. */
static bool s_arg_values_is_valid(const amxc_var_t* values) {
    when_null(values, bad);
    when_false(amxc_var_type_of(values) == AMXC_VAR_ID_HTABLE, bad);

    amxc_var_for_each(it, values) {
        const char* key = amxc_var_key(it);
        bool found = false;
        when_str_empty(key, bad);

        for(const char* const* valid = s_stat_names; *valid != NULL; ++valid) {
            found = strcmp(key, *valid) == 0;
            if(found) {
                break;
            }
        }
        when_false(found, bad);
    }

    return true;
bad:
    return false;
}

static amxd_status_t s_check_origin_access(amxd_object_t* object,
                                           const char* origin) {
    amxd_status_t status = amxd_status_unknown_error;
    char* current_origin = NULL;

    when_str_empty_status(origin, exit, status = amxd_status_parameter_not_found);
    current_origin = amxd_object_get_value(cstring_t, object, PARAM_ORIGIN, &status);
    when_failed(status, exit);
    when_str_empty_status(current_origin, exit, status = amxd_status_parameter_not_found);

    if(strcmp(origin, current_origin) == 0) {
        status = amxd_status_ok;
    } else {
        status = amxd_status_invalid_value;
    }

exit:
    free(current_origin);
    return status;
}

amxd_status_t _setStatistics(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* origin = GET_CHAR(args, ARG_ORIGIN);
    const amxc_var_t* values = amxc_var_get_key(args, ARG_VALUES, AMXC_VAR_FLAG_DEFAULT);
    gmap_mibs_stats_device_info_t* info = s_device_info_for_object(object);
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxc_var_set(bool, ret, false);

    status = amxd_status_missing_key;
    when_str_empty_trace(origin, exit, ERROR, "Missing Origin argument");
    when_null_trace(values, exit, ERROR, "Missing Values argument");
    when_false_status(s_arg_values_is_valid(values), exit, status = amxd_status_invalid_arg);

    status = s_check_origin_access(object, origin);
    when_failed(status, exit);

    status = amxd_status_not_instantiated;
    when_null_trace(info, exit, ERROR, "Missing device entry");

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    status = s_update_values(&trans, values,
                             info->stats_origin == NULL || strcmp(info->stats_origin, origin) != 0);
    when_failed(status, exit);

    status = amxd_trans_apply(&trans, gmap_client_get_mib_server_dm());
    when_failed(status, exit);

    amxc_var_set(bool, ret, true);
    s_device_info_set_stats_origin(info, origin);

exit:
    amxd_trans_clean(&trans);
    return status;
}

amxd_status_t _setStatisticsError(amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* origin = GET_CHAR(args, ARG_ORIGIN);
    gmap_mibs_stats_device_info_t* info = s_device_info_for_object(object);
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxc_var_set(bool, ret, false);

    status = s_check_origin_access(object, origin);
    when_failed(status, exit);

    status = amxd_status_not_instantiated;
    when_null_trace(info, exit, ERROR, "Missing device entry");

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    status = amxd_trans_select_object(&trans, object);
    when_failed(status, exit);
    if((info->stats_origin == NULL) || (strcmp(info->stats_origin, origin) != 0)) {
        s_reset_all_values(&trans, STATUS_ERROR);
    } else {
        status = amxd_trans_set_value(cstring_t, &trans, PARAM_STATUS, STATUS_ERROR);
    }
    when_failed(status, exit);

    status = amxd_trans_apply(&trans, gmap_client_get_mib_server_dm());
    when_failed(status, exit);

    s_device_info_set_error_origin(info, origin);

    amxc_var_set(bool, ret, true);

exit:
    amxd_trans_clean(&trans);
    return status;
}

static amxd_status_t s_reset_dm(amxd_object_t* object, const char* state) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_null_trace(object, exit, ERROR, "NULL parameter");

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    s_reset_all_values(&trans, state);

    status = amxd_trans_apply(&trans, gmap_client_get_mib_server_dm());

exit:
    amxd_trans_clean(&trans);
    return status;
}

void _stats_event_object_changed_origin(const char* const sig_name,
                                        const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    const char* path = GET_CHAR(data, "path");
    const char* new_origin = GETP_CHAR(data, "parameters.Origin.to");
    gmap_mibs_stats_device_info_t* info = s_device_info_for_path(path);
    amxd_object_t* object = NULL;
    bool has_no_stats;
    bool has_no_error;

    when_str_empty_trace(sig_name, exit, ERROR, "NULL/empty");
    when_null_trace(data, exit, ERROR, "NULL");
    when_false(strcmp(sig_name, "dm:object-changed") == 0, exit);
    when_str_empty_trace(path, exit, ERROR, "NULL/empty");
    when_null_trace(new_origin, exit, ERROR, "NULL");
    when_null_trace(info, exit, ERROR, "Missing entry for device '%s'", path);

    has_no_stats = info->stats_origin == NULL || strcmp(info->stats_origin, new_origin) != 0;
    has_no_error = info->error_origin == NULL || strcmp(info->error_origin, new_origin) != 0;

    if(has_no_stats) {
        // The origin has changed and no client has submitted new statistics in the meantime.
        // Therefore, the statistics are still the old statistics from a previous
        // client and should be cleared.
        object = amxd_dm_signal_get_object(gmap_client_get_mib_server_dm(), data);
        when_null_trace(object, exit, WARNING, "Missing object for device '%s'", path);
        // Still retain the error state if it was set though.
        s_reset_dm(object, has_no_error ? STATUS_INIT : STATUS_ERROR);
        s_device_info_set_stats_origin(info, NULL);
    }

    if(has_no_error) {
        s_device_info_set_error_origin(info, NULL);
    }

exit:
    return;
}

