/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define ACTIVE_SOURCE "mib-ip"
#define ACTIVE_PRIORITY 50

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <debug/sahtrace.h>
#include "debug/sahtrace_macros.h"

#include <arpa/inet.h>
#include <gmap/gmap.h>
#include <gmap/extensions/ip/gmap_ext_ipaddress.h>

#include <net/ethernet.h>
#include <ipat/ipat.h>

/**********************************************************
* Macro definitions
**********************************************************/

#define MIB_IP_ARGUMENT_FAMILY "family"

#define MIB_IP_FIELD_IPADDRESS "IPAddress"
#define MIB_IP_FIELD_IPADDRESS_SOURCE "IPAddressSource"
#define MIB_IP_FIELD_IPV4ADDRESS "IPv4Address"
#define MIB_IP_FIELD_IPV6ADDRESS "IPv6Address"
#define MIB_IP_FIELD_STATUS "Status"
#define MIB_IP_FIELD_SCOPE "Scope"
#define MIB_IP_FIELD_ADDRESS_KEY "Key"
#define MIB_IP_FIELD_ADDRESS_SOURCE "AddressSource"
#define MIB_IP_FIELD_ADDRESS "Address"

#define MIB_IP_FIELD_STATUS_VAL_ERROR "error"
#define MIB_IP_FIELD_STATUS_VAL_REACHABLE "reachable"
#define MIB_IP_FIELD_SCOPE_VAL_GLOBAL "global"
#define MIB_IP_FIELD_ADDRESS_SOURCE_VAL_DHCP "DHCP"

#ifndef when_null_status
#define when_null_status(x, l, c) if((x) == NULL) { c; goto l; }
#endif

#define ME "gmap_mibs"

/**********************************************************
* Type definitions
**********************************************************/

typedef struct addresses_info {
    amxd_object_t* dhcp_address;
    amxd_object_t* static_address;
    amxd_object_t* default_address;
    bool active;
} addresses_info_t;

typedef struct _mib_ip {
    amxb_bus_ctx_t* bus_ctx;
} mib_ip_t;

/**********************************************************
* Variable declarations
**********************************************************/

static mib_ip_t mib_ip;

/**********************************************************
* Function Prototypes
**********************************************************/

amxd_status_t _addAddressInstance(amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret);
amxd_status_t _deleteAddressInstance(amxd_object_t* object,
                                     UNUSED amxd_function_t* func,
                                     amxc_var_t* args,
                                     amxc_var_t* ret);
amxd_status_t _setAddressInstance(amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret);
amxd_status_t _getAddress(amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret);
amxd_status_t _ip_address_instance_removed(const char* const sig_name,
                                           const amxc_var_t* const data,
                                           UNUSED void* const priv);
amxd_status_t _ip_address_instance_added(const char* const sig_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* const priv);
amxd_status_t _ip_address_instance_changed(const char* const sig_name,
                                           const amxc_var_t* const data,
                                           UNUSED void* const priv);

/**********************************************************
* Functions
**********************************************************/

static inline bool ip_status_is_reachable(const char* status) {
    return status && (strcmp(status, MIB_IP_FIELD_STATUS_VAL_REACHABLE) == 0);
}

static inline bool ip_status_is_active(const char* status) {
    return status &&
           ((strcmp(status,
                    MIB_IP_FIELD_STATUS_VAL_REACHABLE) == 0) ||
            (strcmp(status, MIB_IP_FIELD_STATUS_VAL_ERROR) == 0));
}

static amxb_bus_ctx_t* local_gmap_client_init(void) {
    if(mib_ip.bus_ctx == NULL) {
        mib_ip.bus_ctx = amxb_be_who_has("Devices.Device");
        gmap_client_init(mib_ip.bus_ctx);
    }
    return mib_ip.bus_ctx;
}

static amxd_status_t addresses_info_get(addresses_info_t* const info,
                                        const amxd_object_t* const addresses) {
    amxd_object_t* address = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    memset(info, 0, sizeof(addresses_info_t));

    amxd_object_for_each(instance, lit, addresses) {
        amxc_var_t param;
        amxc_var_init(&param);
        amxc_var_set_type(&param, AMXC_VAR_ID_HTABLE);
        const char* status_value = NULL;
        const char* scope = NULL;
        const char* source = NULL;
        address = amxc_llist_it_get_data(lit, amxd_object_t, it);

        status = amxd_object_get_params(address, &param, amxd_dm_access_protected);
        if(status != amxd_status_ok) {
            amxc_var_clean(&param);
            SAH_TRACEZ_ERROR(ME, "Error: failed to get parameters from object");
            goto exit;
        }

        status_value = GET_CHAR(&param, MIB_IP_FIELD_STATUS);
        scope = GET_CHAR(&param, MIB_IP_FIELD_SCOPE);
        source = GET_CHAR(&param, MIB_IP_FIELD_ADDRESS_SOURCE);

        if(ip_status_is_active(status_value)) {
            info->active = true;
        }

        if(ip_status_is_reachable(status_value)) {
            if(scope && (strcmp(scope, MIB_IP_FIELD_SCOPE_VAL_GLOBAL) == 0)) {
                if(source &&
                   (strcmp(source, MIB_IP_FIELD_ADDRESS_SOURCE_VAL_DHCP) == 0)) {
                    info->dhcp_address = address;
                }
                info->static_address = address;
            }
            info->default_address = address;
        }
        amxc_var_clean(&param);
    }

    status = amxd_status_ok;
exit:
    return status;
}

static amxd_status_t set_address(amxd_object_t* const object, amxd_object_t* const selected_address) {
    amxd_status_t status = amxd_status_unknown_error;
    char* found_addr = NULL;
    char* found_addr_src = NULL;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, object);
    if(!selected_address) {
        // no addresses available
        // reset ipaddress and ipaddressource to empty
        amxd_trans_set_value(cstring_t, &trans, MIB_IP_FIELD_IPADDRESS, "");
        amxd_trans_set_value(cstring_t, &trans, MIB_IP_FIELD_IPADDRESS_SOURCE, "");
        status = amxd_trans_apply(&trans, gmap_client_get_mib_server_dm());
    } else {
        /* addresses available */
        /* set ipaddress and ipaddressource to selected ipaddress */
        found_addr = amxd_object_get_cstring_t(selected_address,
                                               MIB_IP_FIELD_ADDRESS,
                                               &status);
        when_failed(status, exit);
        found_addr_src = amxd_object_get_cstring_t(selected_address, MIB_IP_FIELD_ADDRESS_SOURCE, &status);
        when_failed(status, exit_free_1);
        amxd_trans_set_value(cstring_t, &trans, MIB_IP_FIELD_IPADDRESS, found_addr);
        amxd_trans_set_value(cstring_t, &trans, MIB_IP_FIELD_IPADDRESS_SOURCE, found_addr_src);
        status = amxd_trans_apply(&trans, gmap_client_get_mib_server_dm());
        free(found_addr_src);
exit_free_1:
        free(found_addr);
    }
exit:
    amxd_trans_clean(&trans);
    return status;
}

static amxd_status_t set_active_state(bool to_active, amxd_object_t* const object) {
    amxd_status_t status = amxd_status_unknown_error;
    char* key = amxd_object_get_cstring_t(object, MIB_IP_FIELD_ADDRESS_KEY, &status);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME,
                         "Error: failed to get param %s from object",
                         MIB_IP_FIELD_ADDRESS_KEY);
        goto exit;
    }

    local_gmap_client_init();/*init gmap_client var if this was not done yet */
    if(!gmap_device_setActive(key, to_active, ACTIVE_SOURCE, ACTIVE_PRIORITY)) {
        SAH_TRACEZ_ERROR(ME, "Error: failed to set active parameter");
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "Successfully set object %s, active parameter to %d", key, to_active);
    status = amxd_status_ok;
exit:
    free(key);
    return status;
}

static amxd_status_t select_address(const bool set_active, amxd_object_t* const object) {
    amxd_status_t status = amxd_status_unknown_error;
    addresses_info_t info;
    amxd_object_t* selected_address = NULL;
    bool set_inactive = true;   /*if no other address is active -> set inactive */

    status = addresses_info_get(&info, amxd_object_get(object, MIB_IP_FIELD_IPV4ADDRESS));
    when_failed(status, exit);
    set_inactive = !info.active;
    selected_address = info.dhcp_address;
    selected_address = selected_address ? selected_address : info.static_address;
    selected_address = selected_address ? selected_address : info.default_address;

    status = addresses_info_get(&info, amxd_object_get(object, MIB_IP_FIELD_IPV6ADDRESS));
    when_failed(status, exit);

    set_inactive &= !info.active;
    selected_address = selected_address ? selected_address : info.dhcp_address;
    selected_address = selected_address ? selected_address : info.static_address;
    selected_address = selected_address ? selected_address : info.default_address;

    /*set the address */
    status = set_address(object, selected_address);
    when_failed(status, exit);

    /* Device is set to active when newly updated ip address is 'reachable' or 'error' */
    /* Device are set inactive when all ip addresses are 'not reacheable' */
    if(set_active) {
        set_active_state(true, object);
    } else if(set_inactive) {
        set_active_state(false, object);
    }

    status = amxd_status_ok;
exit:
    return status;
}

static amxd_status_t update_device_address(const char* const sig_name,
                                           amxd_object_t* const ip_object,
                                           const uint32_t index) {
    amxd_status_t status = amxd_status_unknown_error;
    bool is_active = false;
    amxd_object_t* ip_instance = NULL;
    amxd_object_t* device_object = NULL;
    char* status_value = NULL;

    /*take the parent object */
    device_object = amxd_object_get_parent(ip_object);
    if(device_object == NULL) {
        status = amxd_status_object_not_found;
        SAH_TRACEZ_ERROR(ME, "Error: couldnt get a parent object of the added instance");
        goto exit;
    }

    /*check if the new ipaddress instance, sets the Devices.Device object active */
    if(((strcmp(sig_name,
                "dm:instance-added") == 0) ||
        (strcmp(sig_name, "dm:object-changed") == 0)) && (index != 0)) {

        ip_instance = amxd_object_get_instance(ip_object, NULL, index);
        status_value = amxd_object_get_cstring_t(ip_instance, MIB_IP_FIELD_STATUS, &status);
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME,
                             "Error: could not get the ipaddress instance parameter: %s",
                             MIB_IP_FIELD_STATUS);
            status = amxd_status_parameter_not_found;
            free(status_value);
            goto exit;
        }
        is_active = ip_status_is_active(status_value);
        free(status_value);
    }
    status = select_address(is_active, device_object);

exit:
    return status;
}

static amxd_status_t add_address_information_to_collection(amxc_var_t* const table,
                                                           const amxd_object_t* const addresses) {

    amxd_status_t status = amxd_status_unknown_error;
    const amxc_htable_t* var_table = NULL;

    amxd_object_for_each(instance, it, addresses) {
        amxc_var_t param;
        amxc_var_init(&param);
        amxc_var_set_type(&param, AMXC_VAR_ID_HTABLE);
        amxd_object_t* i = amxc_container_of(it, amxd_object_t, it);

        status = amxd_object_get_params(i, &param, amxd_dm_access_protected);
        if(status != amxd_status_ok) {
            amxc_var_clean(&param);
            SAH_TRACEZ_ERROR(ME, "Error: failed to get parameters from object");
            status = amxd_status_parameter_not_found;
            goto exit;
        }

        var_table = amxc_var_constcast(amxc_htable_t, &param);
        amxc_var_add(amxc_htable_t, table, var_table);

        amxc_var_clean(&param);
    }
    status = amxd_status_ok;
exit:
    return status;
}

static amxd_status_t handle_address_information(const amxd_object_t* const object,
                                                amxc_var_t* const ret,
                                                const char* const family) {

    amxd_object_t* addresses = amxd_object_get(object, family);
    if(!addresses) {
        SAH_TRACEZ_ERROR(ME, "Error: no '%s' field in object found", family);
        return amxd_status_object_not_found;
    }
    return add_address_information_to_collection(ret, addresses);
}

static bool address_in_subnet(char* address, ipat_t* subnet) {
    bool retval = false;
    ipat_t ip = IPAT_INITIALIZE;

    when_false_trace(ipat_pton(&ip, AF_INET_ANY, address), exit, ERROR, "Failed to convert address: %s", address);

    if(ipat_in_subnet(&ip, subnet)) {
        retval = true;
    }

exit:
    ipat_cleanup(&ip);

    return retval;
}

static bool get_ipv6_subnet(ipat_t* subnet, const char* address) {
    bool ret = true;
    ipat_t ip = IPAT_INITIALIZE;
    ipat_t subnet_LLA = IPAT_INITIALIZE;
    ipat_t subnet_ULA = IPAT_INITIALIZE;
    ipat_t subnet_GUA = IPAT_INITIALIZE;

    when_false_trace(ipat_pton(&ip, AF_INET_ANY, address), exit, ERROR, "Failed to convert address: %s", address);
    when_false_trace(ipat_pton(&subnet_GUA, AF_INET_ANY, "2000::/3"), exit, ERROR, "Failed to convert address: %s", address);
    when_false_trace(ipat_pton(&subnet_ULA, AF_INET_ANY, "FC00::/7"), exit, ERROR, "Failed to convert address: %s", address);
    when_false_trace(ipat_pton(&subnet_LLA, AF_INET_ANY, "FE80::/10"), exit, ERROR, "Failed to convert address: %s", address);

    if(ipat_in_subnet(&ip, &subnet_LLA)) {
        ipat_copy(subnet, &subnet_LLA);
    } else if(ipat_in_subnet(&ip, &subnet_ULA)) {
        ipat_copy(subnet, &subnet_ULA);
    } else if(ipat_in_subnet(&ip, &subnet_GUA)) {
        ipat_copy(subnet, &subnet_GUA);
    } else {
        SAH_TRACEZ_ERROR(ME, "Could not locate address' '%s'  subnet range", address);
        ret = false;
    }

exit:
    ipat_cleanup(&ip);
    ipat_cleanup(&subnet_LLA);
    ipat_cleanup(&subnet_ULA);
    ipat_cleanup(&subnet_GUA);
    return ret;
}

/*
 * This should be called after the IPvxAddress instance is added.
 * Otherwise the last instance may not be removed as the new address was not present yet
 */
static int remove_not_reachable(amxd_object_t* object, amxd_object_t* mobject, UNUSED void* priv) {
    amxd_trans_t transaction;
    amxc_var_t* data = (amxc_var_t*) priv;
    uint32_t index = GET_UINT32(data, "index");
    const char* added_address = GET_CHAR(data, "address");
    char* address = amxd_object_get_value(cstring_t, mobject, "Address", NULL);
    amxd_dm_t* const dm = gmap_client_get_mib_server_dm();
    amxc_var_t args;
    amxc_var_t ret;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&transaction);
    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(object, exit, ERROR, "object does not exist");
    when_null_trace(mobject, exit, ERROR, "mobject does not exist");
    when_false_trace(amxd_object_get_type(mobject) == amxd_object_instance, exit, ERROR, "object does not exist");
    when_null_trace(dm, exit, ERROR, "Could not find the gmap 'amxd_dm_t* dm'");

    // should not remove instance that is just added
    when_true(index == amxd_object_get_index(mobject), exit);

    // should not remove ipv6 addresses that are not in the same subnet
    if(strcmp(amxd_object_get_name(object, AMXD_OBJECT_NAMED), MIB_IP_FIELD_IPV6ADDRESS) == 0) {
        ipat_t subnet = IPAT_INITIALIZE;

        when_false(get_ipv6_subnet(&subnet, added_address), exit)
        if(!address_in_subnet(address, &subnet)) {
            // only needs to cleanup subnet when get_ipv6_subnet returns true
            ipat_cleanup(&subnet);
            goto exit;
        }
    }

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", address);
    status = _deleteAddressInstance(object, NULL, &args, &ret);

exit:
    free(address);
    amxd_trans_clean(&transaction);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return status;
}

/*
 * Removes all 'not reachable' ipvx instances, except if this is the last instance of its type.
 * Types = ipv4:["DHCP", "Static"], ipv6:[GUA, ULA, LLA]
 */
static amxd_status_t check_not_reachable_ip_instances(const char* source, const char* address, amxd_object_t* template_object, uint32_t index) {
    amxc_string_t path;
    amxc_var_t args;

    amxc_string_init(&path, 0);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    if(strcmp(amxd_object_get_name(template_object, AMXD_OBJECT_NAMED), MIB_IP_FIELD_IPV4ADDRESS) == 0) {
        amxc_string_setf(&path, ".[AddressSource == '%s' && Status == 'not reachable'].", source);
    } else if(strcmp(amxd_object_get_name(template_object, AMXD_OBJECT_NAMED), MIB_IP_FIELD_IPV6ADDRESS) == 0) {
        amxc_string_setf(&path, ".[Status == 'not reachable'].");
    }

    amxc_var_add_key(cstring_t, &args, "address", address);
    amxc_var_add_key(uint32_t, &args, "index", index);
    amxd_object_for_all(template_object, amxc_string_get(&path, 0), remove_not_reachable, &args);

    amxc_string_clean(&path);
    amxc_var_clean(&args);

    return amxd_status_ok;
}

static amxd_status_t check_is_last_ipv4_instance(amxd_object_t* ipv4_instance) {
    int rv = amxd_status_unknown_error;
    char* type = NULL;
    amxc_llist_t path_list;
    int32_t size = 0;

    amxc_llist_init(&path_list);

    when_null_trace(ipv4_instance, exit, ERROR, "object does not exist");

    type = amxd_object_get_value(cstring_t, ipv4_instance, "AddressSource", NULL);
    when_str_empty_trace(type, exit, ERROR, "Could not get 'AddressSource' parameter value from '%s'", amxd_object_get_path(ipv4_instance, AMXD_OBJECT_TERMINATE));
    amxd_object_resolve_pathf(amxd_object_get_parent(ipv4_instance), &path_list, ".[AddressSource == '%s'].", type);
    free(type);

    size = amxc_llist_size(&path_list);
    if(size < 2) {
        rv = amxd_status_read_only;
    } else {
        rv = amxd_status_ok;
    }

exit:
    amxc_llist_clean(&path_list, amxc_string_list_it_free);

    return rv;
}

static amxd_status_t check_is_last_ipv6_instance(amxd_object_t* ipv6_instance) {
    int rv = amxd_status_unknown_error;
    char* address = amxd_object_get_value(cstring_t, ipv6_instance, "Address", NULL);
    ipat_t subnet = IPAT_INITIALIZE;
    int32_t size = 0;

    when_null_trace(ipv6_instance, exit, ERROR, "object does not exist");
    when_str_empty_trace(address, exit, ERROR, "Could not get 'Address' parameter value from '%s'", amxd_object_get_path(ipv6_instance, AMXD_OBJECT_TERMINATE));

    when_false(get_ipv6_subnet(&subnet, address), exit);
    rv = amxd_status_ok;
    amxd_object_for_each(instance, it, amxd_object_get_parent(ipv6_instance)) {
        amxd_object_t* inst = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* inst_address = amxd_object_get_value(cstring_t, inst, "Address", NULL);

        if(address_in_subnet(inst_address, &subnet)) {
            size++;
        }

        free(inst_address);
        when_true(size >= 2, exit);
    }

    rv = amxd_status_read_only;

exit:
    ipat_cleanup(&subnet);
    free(address);

    return rv;
}

static amxd_object_t* s_find_ip_obj(amxd_object_t* ip_template_obj, const char* ip) {
    when_str_empty_trace(ip, error, ERROR, "NULL/empty argument");
    when_null_trace(ip_template_obj, error, ERROR, "NULL argument");
    when_false_trace(strchr(ip, '\'') == NULL, error, ERROR, "Invalid IP");
    return amxd_object_findf(ip_template_obj, "[Address ^= '%s']", ip);
error:
    return NULL;
}

amxd_status_t _addAddressInstance(amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    amxd_trans_t transaction;
    amxd_status_t retval = amxd_status_parameter_not_found;
    amxd_dm_t* const dm = gmap_client_get_mib_server_dm();

    amxc_var_t* address = amxc_var_get_key(args, "address", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* source = amxc_var_get_key(args, "source", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* reserved = amxc_var_get_key(args, "reserved", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* scope = amxc_var_get_key(args, "scope", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* status = amxc_var_get_key(args, "status", AMXC_VAR_FLAG_DEFAULT);

    amxd_trans_init(&transaction);

    when_null_trace(address, exit, ERROR, "Empty 'address' parameter passed to addAddressInstance");
    when_null_trace(source, exit, ERROR, "Empty 'source' parameter passed to addAddressInstance");

    amxd_trans_select_object(&transaction, object);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_add_inst(&transaction, 0, NULL);
    amxd_trans_set_param(&transaction, "Address", address);
    amxd_trans_set_param(&transaction, "AddressSource", source);
    if(scope != NULL) {
        amxd_trans_set_param(&transaction, "Scope", scope);
    }
    if(status != NULL) {
        amxd_trans_set_param(&transaction, "Status", status);
    }
    if(strcmp(amxd_object_get_name(object, AMXD_OBJECT_NAMED), MIB_IP_FIELD_IPV4ADDRESS) == 0) {
        when_null_trace(reserved, exit, ERROR, "Empty 'reserved' parameter passed to addAddressInstance");
        amxd_trans_set_param(&transaction, "Reserved", reserved);
    }

    retval = amxd_trans_apply(&transaction, dm);

    if(retval == amxd_status_ok) {
        amxc_var_set(bool, ret, true);
    } else {
        amxc_var_set(bool, ret, false);
    }

exit:
    amxd_trans_clean(&transaction);

    return retval;
}

amxd_status_t _deleteAddressInstance(amxd_object_t* object,
                                     UNUSED amxd_function_t* func,
                                     amxc_var_t* args,
                                     amxc_var_t* ret) {
    amxd_trans_t transaction;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_dm_t* const dm = gmap_client_get_mib_server_dm();
    const char* ip = GET_CHAR(args, "address");
    amxd_object_t* ip_obj = s_find_ip_obj(object, ip);
    amxd_object_t* device = amxd_object_get_parent(object);
    const char* key = amxd_object_get_name(device, AMXD_OBJECT_NAMED);

    amxd_trans_init(&transaction);

    if(gmap_device_hasTag(key, "lan physical", NULL, gmap_traverse_down)) {
        if(strcmp(amxd_object_get_name(object, AMXD_OBJECT_NAMED), MIB_IP_FIELD_IPV4ADDRESS) == 0) {
            status = check_is_last_ipv4_instance(ip_obj);
        } else if(strcmp(amxd_object_get_name(object, AMXD_OBJECT_NAMED), MIB_IP_FIELD_IPV6ADDRESS) == 0) {
            status = check_is_last_ipv6_instance(ip_obj);
        }
    }

    if(status == amxd_status_read_only) {
        amxd_trans_select_object(&transaction, ip_obj);

        amxd_trans_set_value(cstring_t, &transaction, "Status", "not reachable");
        status = amxd_trans_apply(&transaction, gmap_client_get_mib_server_dm());
        amxc_var_set(cstring_t, ret, "Deletion is blocked as this is the last instance of its type");

        goto exit;
    }

    amxd_trans_select_object(&transaction, object);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_del_inst(&transaction, amxd_object_get_index(ip_obj), NULL);
    status = amxd_trans_apply(&transaction, dm);

    if(status == amxd_status_ok) {
        amxc_var_set(cstring_t, ret, "OK");
    } else {
        amxc_string_t error_msg;
        amxc_string_init(&error_msg, 0);
        amxc_string_setf(&error_msg, "Deletion transaction failed with error %d", status);
        amxc_var_set(cstring_t, ret, amxc_string_get(&error_msg, 0));
        amxc_string_clean(&error_msg);
    }

exit:
    amxd_trans_clean(&transaction);
    return status;
}

amxd_status_t _setAddressInstance(amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    amxd_trans_t transaction;
    amxd_status_t retval = amxd_status_object_not_found;
    amxd_dm_t* const dm = gmap_client_get_mib_server_dm();
    const char* address = GET_CHAR(args, "address");
    amxd_object_t* instance = amxd_object_findf(object, ".[Address == '%s']", address);

    amxc_var_t* source = amxc_var_get_key(args, "source", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* reserved = amxc_var_get_key(args, "reserved", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* scope = amxc_var_get_key(args, "scope", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* status = amxc_var_get_key(args, "status", AMXC_VAR_FLAG_DEFAULT);

    amxd_trans_init(&transaction);

    amxc_var_set(bool, ret, false);
    when_null_trace(address, exit, ERROR, "Empty 'address' parameter passed to addAddressInstance");
    when_null_trace(instance, exit, ERROR, "No IPvxAddress instance with the address %s", address);

    amxd_trans_select_object(&transaction, instance);

    if(!amxc_var_is_null(source)) {
        amxd_trans_set_param(&transaction, "AddressSource", source);
    }
    if(!amxc_var_is_null(scope)) {
        amxd_trans_set_param(&transaction, "Scope", scope);
    }
    if(!amxc_var_is_null(status)) {
        amxd_trans_set_param(&transaction, "Status", status);
    }
    if((strcmp(amxd_object_get_name(object, AMXD_OBJECT_NAMED), MIB_IP_FIELD_IPV4ADDRESS) == 0) &&
       !amxc_var_is_null(reserved)) {
        amxd_trans_set_param(&transaction, "Reserved", reserved);
    }

    retval = amxd_trans_apply(&transaction, dm);
    if(retval == amxd_status_ok) {
        amxc_var_set(bool, ret, true);
    }

exit:
    amxd_trans_clean(&transaction);

    return retval;
}

amxd_status_t _ip_address_instance_removed(const char* const sig_name,
                                           const amxc_var_t* const data,
                                           UNUSED void* const priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* ip_object = NULL;

    when_null_status(data, exit, status = amxd_status_parameter_not_found);
    if(strcmp(sig_name, "dm:instance-removed") != 0) {
        status = amxd_status_invalid_action;
        goto exit;
    }
    //get the current added ip object
    ip_object = amxd_dm_signal_get_object(gmap_client_get_mib_server_dm(), data);
    when_null_status(ip_object, exit, status = amxd_status_object_not_found);

    status = update_device_address(sig_name, ip_object, 0);

exit:
    return status;
}

amxd_status_t _ip_address_instance_added(const char* const sig_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* const priv) {

    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* ip_object = NULL;
    const char* source = NULL;
    const char* address = NULL;

    when_null_status(data, exit, status = amxd_status_parameter_not_found);
    if(strcmp(sig_name, "dm:instance-added") != 0) {
        status = amxd_status_invalid_action;
        goto exit;
    }
    //get the current added ip object
    ip_object = amxd_dm_signal_get_object(gmap_client_get_mib_server_dm(), data);
    when_null_status(ip_object, exit, status = amxd_status_object_not_found);

    status = update_device_address(sig_name, ip_object, GET_UINT32(data, "index"));
    when_failed(status, exit);

    source = GETP_CHAR(data, "parameters.AddressSource");
    address = GETP_CHAR(data, "parameters.Address");
    when_str_empty_trace(address, exit, ERROR, "Added instance does not contain an address")
    when_str_empty_trace(source, exit, ERROR, "Added instance does not contain a source")
    status = check_not_reachable_ip_instances(source, address, ip_object, GET_UINT32(data, "index"));
exit:
    return status;
}

amxd_status_t _ip_address_instance_changed(const char* const sig_name,
                                           const amxc_var_t* const data,
                                           UNUSED void* const priv) {
    amxd_status_t status = amxd_status_invalid_action;
    amxd_object_t* ip_instance = NULL;
    amxd_object_t* ip_object = NULL;

    when_null_status(data, exit, status = amxd_status_parameter_not_found);
    if(strcmp(sig_name, "dm:object-changed") != 0) {
        goto exit;
    }

    //get the current added ip instance
    ip_instance = amxd_dm_signal_get_object(gmap_client_get_mib_server_dm(), data);
    when_null_status(ip_instance, exit, status = amxd_status_object_not_found);

    /*get the ip object */
    ip_object = amxd_object_get_parent(ip_instance);
    when_null_status(ip_object, exit, status = amxd_status_object_not_found);

    status = update_device_address(sig_name, ip_object, amxd_object_get_index(ip_instance));
exit:
    return status;
}

amxd_status_t _getAddress(amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    bool family_shown = false;
    cstring_t family = amxc_var_dyncast(cstring_t, GET_ARG(args, MIB_IP_ARGUMENT_FAMILY));

    when_null_status(ret, exit, status = amxd_status_parameter_not_found);
    when_null_status(object, exit, status = amxd_status_parameter_not_found);
    when_null_status(args, exit, status = amxd_status_parameter_not_found);

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);

    /*if valid arg, add ipv4 addresses to list */
    if((family == NULL) || !(*family) || (gmap_ip_family_string2id(family) == AF_INET)) {
        family_shown = true;
        status = handle_address_information(object, ret, MIB_IP_FIELD_IPV4ADDRESS);
        when_failed(status, exit);
    }

    /*add ipv6 addresses to list */
    if((family == NULL) || !(*family) || (gmap_ip_family_string2id(family) == AF_INET6)) {
        family_shown = true;
        status = handle_address_information(object, ret, MIB_IP_FIELD_IPV6ADDRESS);
        when_failed(status, exit);
    }

    if(family && *family && !family_shown) { /*if family_shown is false and an argument "family"
                                              * is given -> invalid arg */
        status = amxd_status_invalid_value;
        SAH_TRACEZ_ERROR(ME, "Error: invalid argument given");
        goto exit;
    }
    status = amxd_status_ok;
exit:
    free(family);
    return status;
}
