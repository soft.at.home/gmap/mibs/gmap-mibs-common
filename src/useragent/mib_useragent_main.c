/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_expression.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <amxp/amxp.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <arpa/inet.h>
#include <gmap/gmap.h>

#include <mib_useragent.h>

/**********************************************************
* Macro definitions
**********************************************************/
#define ME "gmap_mibs"
#define MIB_NAME "UserAgents"
#define USERAGENT_PATH "EmbeddedFiltering.Logging.Useragent."

/**********************************************************
* Type definitions
**********************************************************/
amxb_subscription_t* useragent_subscription_add = NULL;
amxc_htable_t entry_table;
bool embedded_disconnected = false;

/**********************************************************
* Variable declarations
**********************************************************/

amxb_bus_ctx_t* gmap_bus = NULL;

/**********************************************************
* Function Prototypes
**********************************************************/
amxd_status_t _setUserAgent(amxd_object_t* object,
                            amxd_function_t* func,
                            amxc_var_t* args,
                            amxc_var_t* ret);

amxd_status_t _removeUserAgent(amxd_object_t* object,
                               amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret);

/**********************************************************
* Functions
**********************************************************/

static void local_gmap_client_init(void) {
    if(gmap_bus == NULL) {
        gmap_bus = amxb_be_who_has("Devices.Device.");
        gmap_client_init(gmap_bus);
    }
}

void mib_useragent_init(UNUSED const char* signame,
                        UNUSED const amxc_var_t* const data,
                        UNUSED void* const priv) {
    amxb_bus_ctx_t* useragent_ctx = NULL;
    amxc_string_t expr;
    int ret = -1;

    amxc_string_init(&expr, 0);

    amxp_slot_disconnect_all(mib_useragent_init);
    embedded_disconnected = true;
    local_gmap_client_init();

    useragent_ctx = amxb_be_who_has(USERAGENT_PATH);
    when_null_trace(useragent_ctx, exit, ERROR, "Could not find %s", USERAGENT_PATH);

    amxc_string_appendf(&expr, "notification == 'UserAgentDetected' && path == '%s'", USERAGENT_PATH);
    ret = amxb_subscription_new(&useragent_subscription_add,
                                useragent_ctx,
                                USERAGENT_PATH,
                                amxc_string_get(&expr, 0),
                                mib_useragent_add_instance,
                                NULL);
    when_failed_trace(ret, exit, ERROR, "Could not subscribe to the %s object", USERAGENT_PATH);

exit:
    amxc_string_clean(&expr);
}

CONSTRUCTOR static void mib_start(void) {
    int r = AMXB_ERROR_UNKNOWN;
    r = amxp_slot_connect_filtered(NULL, "^wait:EmbeddedFiltering\\.Logging\\.Useragent\\.$", NULL, mib_useragent_init, NULL);
    when_failed_trace(r, exit, ERROR, "Failed to wait for EmbeddedFiltering.Logging.Useragent. to be available");
    amxb_wait_for_object("EmbeddedFiltering.Logging.Useragent.");
exit:
    return;
}

DESTRUCTOR static void mib_end(void) {
    amxb_subscription_delete(&useragent_subscription_add);
    if(!embedded_disconnected) {
        amxp_slot_disconnect_all(mib_useragent_init);
    }
}

amxd_status_t _setUserAgent(amxd_object_t* object,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* device = amxd_object_get_value(cstring_t, object, "Alias", NULL);
    const char* useragent = GET_CHAR(args, "useragent");
    const char* type = GET_CHAR(args, "type");
    const char* source = GET_CHAR(args, "source");

    status = add_user_agent(device, useragent, type, source);

    if(status == amxd_status_ok) {
        amxc_var_set(bool, ret, true);
    }

    free((char*) device);
    return status;
}

amxd_status_t _removeUserAgent(amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxp_expr_status_t expr_status = amxp_expr_status_unknown_error;
    const char* useragent = GET_CHAR(args, "useragent");
    const char* type = GET_CHAR(args, "type");
    const char* source = GET_CHAR(args, "source");
    const char* dev_name = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    amxp_expr_t expr;
    amxd_object_t* useragents_object = NULL;
    amxd_object_t* info_obj = NULL;

    amxc_var_set(bool, ret, false);
    expr_status = amxp_expr_buildf(&expr, "UserAgent == '%s' and Type == '%s' and Source == '%s'", useragent, type, source);
    when_failed_trace(expr_status, exit, ERROR, "Build expression for useragent %s failed: %d", useragent, expr_status);

    /* Check if the arguments are correct */
    status = amxd_status_missing_key;
    when_str_empty(useragent, exit);
    when_str_empty(type, exit);
    when_str_empty(source, exit);

    /* check if the useragent is one of the existing mibs */
    useragents_object = amxd_object_get_child(object, MIB_NAME);
    status = amxd_status_object_not_found;
    when_null_trace(useragents_object, exit, ERROR, "%s object not found", MIB_NAME);

    /* Check if the instance exists, if it does remove it*/
    info_obj = amxd_object_find_instance(useragents_object, &expr);
    when_null_trace(info_obj, exit, ERROR, "Object with useragent %s not found", useragent);
    int index = amxd_object_get_index(info_obj);
    status = gmap_delete_instance(dev_name, "UserAgents", index);
    when_null_trace(info_obj, exit, ERROR, "Could not delete useragent with index %d", index);

    amxc_var_set(bool, ret, true);
    status = amxd_status_ok;

exit:
    amxp_expr_clean(&expr);
    return status;
}
