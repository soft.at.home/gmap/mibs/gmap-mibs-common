/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <arpa/inet.h>
#include <gmap/gmap.h>

#include <mib_useragent.h>


/**********************************************************
* Macro definitions
**********************************************************/
#define ME "gmap_mibs"
#define MIB_NAME "UserAgents"
#define MAX_USERAGENTS 10


static amxd_status_t remove_oldest_useragent(const char* dev_name, amxc_var_t* useragents) {
    amxc_var_t* oldest_useragent = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_string_t path;
    amxc_string_t* index;
    amxc_llist_t word_list;
    amxc_llist_it_t* it;

    amxc_string_init(&path, 0);
    amxc_llist_init(&word_list);

    amxc_var_for_each(var, useragents) {
        const amxc_ts_t* oldest = NULL;
        const amxc_ts_t* current = NULL;
        amxc_var_t* oldest_seen = NULL;
        amxc_var_t* last_seen = NULL;

        if(oldest_useragent == NULL) {
            oldest_useragent = var;
        }

        oldest_seen = amxc_var_get_key(oldest_useragent, "LastSeen", AMXC_VAR_FLAG_DEFAULT);
        last_seen = amxc_var_get_key(var, "LastSeen", AMXC_VAR_FLAG_DEFAULT);

        oldest = amxc_var_constcast(amxc_ts_t, oldest_seen);
        current = amxc_var_constcast(amxc_ts_t, last_seen);

        if(oldest->sec > current->sec) {
            oldest_useragent = var;
        }
    }

    amxc_string_set(&path, amxc_var_key(oldest_useragent));
    amxc_string_split_to_llist(&path, &word_list, '.');

    it = amxc_llist_it_get_previous(amxc_llist_get_last(&word_list));
    index = amxc_string_from_llist_it(it);
    when_false_trace(amxc_string_is_numeric(index), exit, ERROR, "The useragent path is incorrect: %s", amxc_string_get(&path, 0));

    status = gmap_delete_instance(dev_name, "UserAgents", atoi(amxc_string_get(index, 0)));

exit:
    amxc_string_clean(&path);
    amxc_llist_clean(&word_list, amxc_string_list_it_free);
    return status;
}

static amxd_status_t manage_max_nr_of_useragents(const char* dev_name) {
    int len = 0;
    amxc_var_t ret;
    amxc_var_t* useragents = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&ret);

    status = gmap_get_subobject_instance(dev_name, "UserAgents.*", 0, &ret);
    when_failed_trace(status, exit, ERROR, "Could not get the subobject instances for 'UserAgents.*'");

    useragents = GETI_ARG(&ret, 0);
    len = amxc_htable_size(amxc_var_constcast(amxc_htable_t, useragents));

    if(len >= MAX_USERAGENTS) {
        status = remove_oldest_useragent(dev_name, useragents);
        when_failed_trace(status, exit, ERROR, "Could not delete the oldest UserAgents instance");
    }

exit:
    amxc_var_clean(&ret);
    return amxd_status_ok;
}

amxd_status_t add_user_agent(const char* dev_name, const char* useragent, const char* type, const char* source) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t values;
    amxc_var_t ret;
    amxc_var_t* retval_obj = NULL;
    amxc_string_t path;

    amxc_var_init(&values);
    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    /* Check if the arguments are correct */
    status = amxd_status_missing_key;
    when_str_empty(useragent, exit);
    when_str_empty(type, exit);
    when_str_empty(source, exit);

    amxc_var_add_key(cstring_t, &values, "UserAgent", useragent);
    amxc_var_add_key(cstring_t, &values, "Type", type);
    amxc_var_add_key(cstring_t, &values, "Source", source);

    if(amxb_be_who_has("Time.")) {
        amxc_ts_t now;
        amxc_ts_now(&now);
        amxc_var_add_key(amxc_ts_t, &values, "LastSeen", &now);
    }

    amxc_string_appendf(&path, "UserAgents.[UserAgent == '%s' && Type == '%s' && Source == '%s']", useragent, type, source);
    gmap_get_subobject_instance(dev_name, amxc_string_get(&path, 0), 0, &ret);
    retval_obj = GETI_ARG(&ret, 0);

    status = amxd_status_ok;
    if(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, retval_obj))) {
        int index = 0;

        manage_max_nr_of_useragents(dev_name);
        index = gmap_add_instance(dev_name, "UserAgents", &values);

        if(index == 0) {
            status = amxd_status_unknown_error;
        }

    } else {
        status = amxd_status_duplicate;
    }
exit:
    amxc_string_clean(&path);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
    return status;
}

void mib_useragent_add_instance(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                UNUSED void* const priv) {
    const char* dev_name = GET_CHAR(data, "Device");
    const char* useragent = GET_CHAR(data, "UserAgent");
    const char* type = GET_CHAR(data, "Type");

    add_user_agent(dev_name, useragent, type, "useragent");
}
