/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_variant.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_object_expression.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>

#include <gmap/gmap.h>

#include "mib_priority.h"
#include "test_priority.h"
#include "../common/dummy.h"

/**********************************************************
* Variable declarations
**********************************************************/
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxd_object_t* dev_obj = NULL;
static amxd_dm_t* localdm = NULL;
static amxo_parser_t* localparser = NULL;


static bool load_mib_module() {
    dev_obj = amxd_object_findf(&(localdm->object), "Devices.Device.temp.");

    assert_int_equal(amxo_resolver_ftab_add(localparser, "setConfig", AMXO_FUNC(_setConfig)), 0);
    assert_int_equal(amxo_resolver_ftab_add(localparser, "setPriority", AMXO_FUNC(_setPriority)), 0);

    amxo_parser_scan_mib_dir(localparser, "../../mibs");
    amxo_parser_load_mib(localparser, localdm, "priority");
    amxo_parser_apply_mib(localparser, dev_obj, "priority");

    return true;
}

int test_priority_setup(UNUSED void** state) {
    amxc_var_t* var = NULL;
    localdm = test_get_dm();
    localparser = test_get_parser();

    gmap_client_set_mib_server_dm(localdm);
    assert_int_equal(test_register_dummy(), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    gmap_client_init(bus_ctx);

    // do not load import so files
    var = amxo_parser_claim_config(localparser, "odl-import");
    amxc_var_set(bool, var, false);

    assert_int_equal(test_load_dummy_remote("../common/dummy_gmap.odl"), 0);
    assert_int_equal(test_load_dummy_remote("../../config/gmap_conf_priority.odl"), 0);

    load_mib_module();
    handle_events();

    return amxd_status_ok;
}

int test_priority_teardown(UNUSED void** state) {
    amxo_parser_remove_mibs(test_get_parser(), dev_obj, NULL);
    amxd_dm_clean(localdm);
    amxo_parser_clean(localparser);
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);
    assert_int_equal(test_unregister_dummy(), 0);

    return amxd_status_ok;
}

void test_set_config(UNUSED void** state) {
    amxd_object_t* priority = amxd_object_findf(dev_obj, "Priority");
    amxc_var_t args;
    amxc_var_t ret;
    char* config = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    assert_non_null(priority);

    // Check default value
    config = amxd_object_get_value(cstring_t, priority, "Configuration", NULL);
    assert_string_equal(config, "Automatic");
    free(config);

    // Fails when mandatory arg is missing
    assert_int_equal(amxd_object_invoke_function(priority, "setConfig", &args, &ret), amxd_status_invalid_function_argument);

    // Fails when arg does not match supported options
    amxc_var_add_key(cstring_t, &args, "config", "Invalid");
    assert_int_equal(amxd_object_invoke_function(priority, "setConfig", &args, &ret), amxd_status_invalid_value);

    // Success
    amxc_var_add_key(cstring_t, &args, "config", "Manual");
    assert_int_equal(amxd_object_invoke_function(priority, "setConfig", &args, &ret), amxd_status_ok);
    handle_events();

    // Check changed value
    config = amxd_object_get_value(cstring_t, priority, "Configuration", NULL);
    assert_string_equal(config, "Manual");

    free(config);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}

void test_set_priority(UNUSED void** state) {
    amxd_object_t* priority = amxd_object_findf(dev_obj, "Priority");
    amxc_var_t args;
    amxc_var_t ret;
    char* type = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    assert_non_null(priority);

    // Check default value
    type = amxd_object_get_value(cstring_t, priority, "Type", NULL);
    assert_string_equal(type, "BestEffort");
    free(type);

    // Fails when mandatory arg is missing
    assert_int_equal(amxd_object_invoke_function(priority, "setPriority", &args, &ret), amxd_status_invalid_function_argument);

    // Fails when arg does not match supported options
    amxc_var_add_key(cstring_t, &args, "type", "Invalid");
    assert_int_equal(amxd_object_invoke_function(priority, "setPriority", &args, &ret), amxd_status_invalid_value);

    // Success
    amxc_var_add_key(cstring_t, &args, "type", "Gaming");
    assert_int_equal(amxd_object_invoke_function(priority, "setPriority", &args, &ret), amxd_status_ok);
    handle_events();

    // Check changed value
    type = amxd_object_get_value(cstring_t, priority, "Type", NULL);
    assert_string_equal(type, "Gaming");

    free(type);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}
