/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_useragent.h"
#include "../common/dummy.h"
#include "mib_useragent.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_variant.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_object_expression.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>

#include <gmap/gmap.h>

amxd_status_t _setUserAgent(amxd_object_t* object,
                            amxd_function_t* func,
                            amxc_var_t* args,
                            amxc_var_t* ret);

amxd_status_t _removeUserAgent(amxd_object_t* object,
                               amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret);

/**********************************************************
* Macro definitions
**********************************************************/

#define MIB_USERAGENT_INSTANCE "UserAgents"
#define MIB_USERAGENT_PARAMETER "UserAgent"

/**********************************************************
* Variable declarations
**********************************************************/
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxd_object_t* dev_obj = NULL;
static amxd_dm_t* localdm = NULL;
static amxo_parser_t* localparser = NULL;

static void create_8_useragents() {
    char* useragent = "agent";
    char* type = "sah";
    char* source = "test_useragent";

    for(int i = 3; i <= 10; i++) {
        amxc_string_t useragent_string;
        amxc_string_t type_string;
        amxc_string_t source_string;
        amxc_var_t args;
        amxc_var_t ret;

        amxc_var_init(&args);
        amxc_var_init(&ret);

        amxc_string_init(&useragent_string, 0);
        amxc_string_init(&type_string, 0);
        amxc_string_init(&source_string, 0);

        amxc_string_appendf(&useragent_string, "%s%d", useragent, i);
        amxc_string_appendf(&type_string, "%s%d", type, i);
        amxc_string_appendf(&source_string, "%s%d", source, i);

        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "useragent", amxc_string_get(&useragent_string, 0));
        amxc_var_add_key(cstring_t, &args, "type", amxc_string_get(&type_string, 0));
        amxc_var_add_key(cstring_t, &args, "source", amxc_string_get(&source_string, 0));

        assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                     "setUserAgent",
                                                     &args,
                                                     &ret),
                         amxd_status_ok);

        assert_true(amxc_var_constcast(bool, &ret));

        amxc_var_clean(&ret);
        amxc_var_clean(&args);
        amxc_string_clean(&source_string);
        amxc_string_clean(&type_string);
        amxc_string_clean(&useragent_string);
    }
}

static int get_useragents_len(char* dev_name) {
    int len = 0;
    amxc_var_t ret;

    amxc_var_init(&ret);

    gmap_get_subobject_instance(dev_name, "UserAgents.*", 0, &ret);
    len = amxc_htable_size(amxc_var_constcast(amxc_htable_t, GETI_ARG(&ret, 0)));

    amxc_var_clean(&ret);
    return len;
}

static bool load_mib_module() {
    dev_obj = amxd_object_findf(&(localdm->object), "Devices.Device.temp.");

    assert_int_equal(amxo_resolver_ftab_add(localparser, "setUserAgent", AMXO_FUNC(_setUserAgent)), 0);
    assert_int_equal(amxo_resolver_ftab_add(localparser, "removeUserAgent", AMXO_FUNC(_removeUserAgent)), 0);

    amxo_parser_scan_mib_dir(localparser, "../../mibs");
    amxo_parser_load_mib(localparser, localdm, "useragent");
    amxo_parser_apply_mib(localparser, dev_obj, "useragent");

    return true;
}

int test_useragent_setup(AMXB_UNUSED void** state) {
    amxc_var_t* var = NULL;
    localdm = test_get_dm();
    localparser = test_get_parser();

    assert_int_equal(test_register_dummy(), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    gmap_client_init(bus_ctx);

    // do not load import so files
    var = amxo_parser_claim_config(localparser, "odl-import");
    amxc_var_set(bool, var, false);

    assert_int_equal(test_load_dummy_remote("../common/dummy_gmap.odl"), 0);
    assert_int_equal(test_load_dummy_remote("embeddedfiltering.odl"), 0);

    mib_useragent_init(NULL, NULL, NULL);

    load_mib_module();
    handle_events();

    return amxd_status_ok;
}

int test_useragent_teardown(AMXB_UNUSED void** state) {
    amxo_parser_remove_mibs(test_get_parser(), dev_obj, NULL);
    amxd_dm_clean(localdm);
    amxo_parser_clean(localparser);
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);
    assert_int_equal(test_unregister_dummy(), 0);

    return amxd_status_ok;
}

void test_set_useragent(AMXB_UNUSED void** state) {
    /*
     * Add a useragent and check that it worked
     * As the Time. plugin has not yet started, the 'LastSeen' param should be the epoch
     */
    char* useragent = "agent";
    char* type = "sah";
    char* source = "test_useragent";
    char* return_value = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_USERAGENT_INSTANCE);
    amxd_object_t* instance = NULL;

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "useragent", useragent);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setUserAgent",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_true(amxc_var_constcast(bool, &ret));

    amxp_expr_buildf(&expr, "UserAgent == '%s' and Type == '%s' and Source == '%s'", useragent, type, source);
    instance = amxd_object_find_instance(type_collection, &expr);
    assert_non_null(instance);

    return_value = amxd_object_get_value(cstring_t, instance, "LastSeen", NULL);
    assert_string_equal(return_value, "0001-01-01T00:00:00Z");

    free(return_value);
    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_same_useragent_value(AMXB_UNUSED void** state) {
    /*
     * Add a useragent with the same value as the existing instance, but with a different type and source
     * This should create a new instance
     */
    char* useragent = "agent";
    char* type = "sah1";
    char* source = "test_useragent1";
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_USERAGENT_INSTANCE);
    amxd_object_t* instance = NULL;

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "useragent", useragent);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setUserAgent",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_true(amxc_var_constcast(bool, &ret));

    amxp_expr_buildf(&expr, "UserAgent == '%s' and Source == '%s'", useragent, source);
    instance = amxd_object_find_instance(type_collection, &expr);
    assert_non_null(instance);

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_last_seen_param(AMXB_UNUSED void** state) {
    /*
     * Start the Time. plugin and check that by creating or updating an existing
     * instance, the lastSeen param is set to the current time.
     */
    char* useragent = "agent2";
    char* type = "sah2";
    char* source = "test_useragent2";
    amxc_ts_t* return_value = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_USERAGENT_INSTANCE);
    amxd_object_t* root_obj = NULL;
    amxd_object_t* instance = NULL;
    amxo_parser_t* parser = test_get_parser();
    amxd_dm_t* dm = test_get_dm();

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_null(amxb_be_who_has("Time."));
    amxc_ts_t older_time;
    amxc_ts_now(&older_time);

    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);
    assert_int_equal(amxo_parser_parse_file(parser, "time.odl", root_obj), 0);

    assert_non_null(amxb_be_who_has("Time."));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "useragent", useragent);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setUserAgent",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_true(amxc_var_constcast(bool, &ret));

    amxp_expr_buildf(&expr, "UserAgent == '%s' and Source == '%s'", useragent, source);
    instance = amxd_object_find_instance(type_collection, &expr);
    assert_non_null(instance);

    return_value = amxd_object_get_value(amxc_ts_t, instance, "LastSeen", NULL);
    assert_int_equal(amxc_ts_compare(return_value, &older_time), 1);

    free(return_value);
    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_same_useragent(AMXB_UNUSED void** state) {
    /*
     * Request to setUserAgent, but with an already existing instance.
     * This should not create a second instance but update the lastSeen param;
     * As the time plugin has been started by now.
     */
    int instance_count = 0;
    char* useragent = "agent";
    char* type = "sah";
    char* source = "test_useragent";
    amxc_ts_t older_time;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* instance = NULL;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_USERAGENT_INSTANCE);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_ts_now(&older_time);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "useragent", useragent);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setUserAgent",
                                                 &args,
                                                 &ret),
                     amxd_status_duplicate);

    assert_false(amxc_var_constcast(bool, &ret));

    amxp_expr_buildf(&expr, "UserAgent == '%s' and Source == '%s' and Type == '%s'", useragent, source, type);
    instance = amxd_object_find_instance(type_collection, &expr);
    assert_non_null(instance);

    // Check that there is exactly one instance
    amxd_object_for_each(instance, it, type_collection) {
        amxd_object_t* inst = amxc_llist_it_get_data(it, amxd_object_t, it);
        const char* current_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(inst, "UserAgent"));

        if(strcmp(current_name, useragent) == 0) {
            instance_count++;
        }
    }

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_no_useragent(AMXB_UNUSED void** state) {
    /*
     * Check that setUserAgent cannot be executed when missing the 'useragent' param
     */
    char* useragent = "";
    char* type = "sah";
    char* source = "test_useragent";
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "useragent", useragent);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setUserAgent",
                                                 &args,
                                                 &ret),
                     amxd_status_missing_key);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_no_type(AMXB_UNUSED void** state) {
    /*
     * Check that setUserAgent cannot be executed when missing the 'type' param
     */
    char* useragent = "agent1";
    char* type = "";
    char* source = "test_useragent";
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "useragent", useragent);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setUserAgent",
                                                 &args,
                                                 &ret),
                     amxd_status_missing_key);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_no_source(AMXB_UNUSED void** state) {
    /*
     * Check that setUserAgent cannot be executed when missing the 'source' param
     */
    char* useragent = "agent1";
    char* type = "sah";
    char* source = "";
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "useragent", useragent);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setUserAgent",
                                                 &args,
                                                 &ret),
                     amxd_status_missing_key);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_remove_useragent(AMXB_UNUSED void** state) {
    /*
     * Check that an instance is correctly removed by calling removeUserAgent on it
     */
    char* useragent = "agent";
    char* type = "sah";
    char* source = "test_useragent";
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_USERAGENT_INSTANCE);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxp_expr_buildf(&expr, "UserAgent == '%s' and Type == '%s' and Source == '%s'", useragent, type, source);
    assert_non_null(amxd_object_find_instance(type_collection, &expr));
    amxp_expr_clean(&expr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "useragent", useragent);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "source", source);


    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "removeUserAgent",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_true(amxc_var_constcast(bool, &ret));

    type_collection = amxd_object_get_child(dev_obj, MIB_USERAGENT_INSTANCE);
    amxp_expr_buildf(&expr, "UserAgent == '%s' and Type == '%s' and Source == '%s'", useragent, type, source);
    assert_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_remove_same_useragent(AMXB_UNUSED void** state) {
    /*
     * Check that the second instance is correctly removed by calling removeUserAgent on it
     * This test also makes sure that the removal can differentiate between the two
     * similar useragents.
     */
    char* useragent = "agent";
    char* type = "sah1";
    char* source = "test_useragent1";
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_USERAGENT_INSTANCE);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxp_expr_buildf(&expr, "UserAgent == '%s' and Type == '%s' and Source == '%s'", useragent, type, source);
    assert_non_null(amxd_object_find_instance(type_collection, &expr));
    amxp_expr_clean(&expr);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "useragent", useragent);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "source", source);


    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "removeUserAgent",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_true(amxc_var_constcast(bool, &ret));

    amxp_expr_buildf(&expr, "UserAgent == '%s' and Type == '%s' and Source == '%s'", useragent, type, source);
    assert_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_adds_useragent_from_event(AMXB_UNUSED void** state) {
    /*
     * The useragent mib should be able to catch the "UserAgentDetected" event.
     * From this event it should extract the correct info and create a new useragent.
     */
    char* useragent = "agent";
    char* type = "sah";
    char* device = "temp";
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_USERAGENT_INSTANCE);
    amxd_object_t* instance = NULL;
    amxd_object_t* const object = amxd_object_findf(&(test_get_dm()->object), "EmbeddedFiltering.Logging.Useragent.");

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "UserAgent", useragent);
    amxc_var_add_key(cstring_t, &args, "Type", type);
    amxc_var_add_key(cstring_t, &args, "Device", device);

    /* this sleep is added to have a notable difference in time between  this useragent
     * and the useragent from test_last_seen_param
     */
    sleep(1);

    amxd_object_emit_signal(object, "UserAgentDetected", &args);
    handle_events();

    amxp_expr_buildf(&expr, "UserAgent == '%s' and Type == '%s' and Source == 'useragent'", useragent, type);
    instance = amxd_object_find_instance(type_collection, &expr);
    assert_non_null(instance);

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_max_nr_of_useragents(AMXB_UNUSED void** state) {
    /*
     * This test checks that the max nr of useragents cannot be exceeded.
     * Whenever the limit would be exceeded. The oldest agent should be removed,
     * to make place for the new one.
     */
    char* dev_name = "temp";
    char* useragent = "agent11";
    char* type = "sah11";
    char* source = "test_useragent11";
    amxc_var_t ret;
    amxc_var_t args;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_USERAGENT_INSTANCE);
    amxd_object_t* instance = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&args);

    /* Check that the original length is 2 */
    assert_int_equal(get_useragents_len(dev_name), 2);

    create_8_useragents();

    /* Check that 8 useragent have been added */
    assert_int_equal(get_useragents_len(dev_name), 10);

    /* Add a useragent that would cause the max nr to be exceeded */
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "useragent", useragent);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setUserAgent",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    /* Max useragent limit is not exceeded */
    assert_true(amxc_var_constcast(bool, &ret));
    assert_int_equal(get_useragents_len(dev_name), 10);

    /* New useragent has been added */
    amxp_expr_buildf(&expr, "UserAgent == '%s' and Type == '%s' and Source == '%s'", useragent, type, source);
    instance = amxd_object_find_instance(type_collection, &expr);
    assert_non_null(instance);
    amxp_expr_clean(&expr);

    /* Oldest useragent has been removed */
    amxp_expr_buildf(&expr, "UserAgent == '%s' and Type == '%s' and Source == '%s'", "agent2", "sah2", "test_useragent2");
    instance = amxd_object_find_instance(type_collection, &expr);
    assert_null(instance);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxp_expr_clean(&expr);
}
