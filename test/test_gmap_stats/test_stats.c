/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_stats.h"
#include "../common/dummy.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_variant.h>
#include <amxc/amxc_timestamp.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_expression.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>

#include <gmap/gmap.h>

amxd_status_t _setStatistics(amxd_object_t* object,
                             amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret);
amxd_status_t _setStatisticsError(amxd_object_t* object,
                                  amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret);

void _stats_event_object_added(const char* const sig_name,
                               const amxc_var_t* const data,
                               void* const priv);
void _stats_event_object_removed(const char* const sig_name,
                                 const amxc_var_t* const data,
                                 void* const priv);
void _stats_event_object_changed_origin(const char* const sig_name,
                                        const amxc_var_t* const data,
                                        void* const priv);

int __wrap_amxc_ts_now(amxc_ts_t* tsp);

/**********************************************************
* Macro definitions
**********************************************************/

#define MIB_NAME "stats"
#define PARAM_COLLECTION_INTERVAL "CollectionInterval"
#define PARAM_ORIGIN "Origin"
#define PARAM_LAST_UPDATE "LastUpdate"
#define PARAM_INITIAL_TIME "InitialTime"
#define PARAM_STATUS "Status"
#define STATUS_INIT "Init"
#define STATUS_SUCCESS "Success"
#define STATUS_ERROR "Error"
#define ARG_ORIGIN "origin"
#define ARG_VALUES "values"
#define ORIGIN_ETH "eth_switch"
#define ORIGIN_WIFI "wifi"


/**********************************************************
* Variable declarations
**********************************************************/
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxd_object_t* dev_obj = NULL;


/**********************************************************
* Function definitions
**********************************************************/
static bool load_mib_module() {
    amxo_parser_t* parser = test_get_parser();
    amxd_dm_t* dm = test_get_dm();
    dev_obj = amxd_object_findf(&dm->object, "Devices.Device.temp.");

    assert_int_equal(amxo_resolver_ftab_add(parser, "setStatistics", AMXO_FUNC(_setStatistics)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setStatisticsError", AMXO_FUNC(_setStatisticsError)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_event_object_added", AMXO_FUNC(_stats_event_object_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_event_object_removed", AMXO_FUNC(_stats_event_object_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "stats_event_object_changed_origin", AMXO_FUNC(_stats_event_object_changed_origin)), 0);

    amxo_parser_scan_mib_dir(parser, "../../mibs");
    amxo_parser_load_mib(parser, dm, MIB_NAME);

    amxo_parser_apply_mib(parser, dev_obj, MIB_NAME);

    return true;
}

int test_stats_setup(AMXB_UNUSED void** state) {
    amxc_var_t* var = NULL;
    amxo_parser_t* parser = NULL;

    assert_int_equal(test_register_dummy(), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    gmap_client_init(bus_ctx);

    // parser and dm are created in dummy connect
    parser = test_get_parser();

    assert_int_equal(amxo_resolver_ftab_add(parser, "load", AMXO_FUNC(_load)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "get", AMXO_FUNC(_get)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setTag", AMXO_FUNC(_setTag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "clearTag", AMXO_FUNC(_clearTag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setActive", AMXO_FUNC(_setActive)), 0);

    gmap_client_set_mib_server_dm(test_get_dm());

    // do not load import so files
    var = amxo_parser_claim_config(parser, "odl-import");
    amxc_var_set(bool, var, false);

    assert_int_equal(test_load_dummy_remote("../common/dummy_gmap.odl"), 0);

    load_mib_module();

    return amxd_status_ok;
}

int test_stats_teardown(AMXB_UNUSED void** state) {
    amxd_object_remove_mib(dev_obj, "stats");

    handle_events();

    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);
    assert_int_equal(test_unregister_dummy(), 0);

    return amxd_status_ok;
}

int __wrap_amxc_ts_now(amxc_ts_t* tsp) {
    amxc_ts_t* mocked = NULL;

    assert_non_null(tsp);
    mocked = mock_ptr_type(amxc_ts_t*);
    assert_non_null(mocked);
    *tsp = *mocked;
    return 0;
}


/** @brief Initializes the timestamp struct to the default value used in libamxo and libamxc */
static void s_init_timestamp_default(amxc_ts_t* timestamp) {
    amxc_var_t variant = { 0 };
    amxc_var_init(&variant);

    assert_non_null(timestamp);
    amxc_var_set_type(&variant, AMXC_VAR_ID_TIMESTAMP);
    *timestamp = *amxc_var_constcast(amxc_ts_t, &variant);

    amxc_var_clean(&variant);
}

static void _assert_object_timestamp_cmp(amxd_object_t* object, const char* key, const amxc_ts_t* expected, int cmp, const char* file, int line) {
    assert_non_null(object);
    assert_non_null(key);
    assert_string_not_equal(key, "");
    assert_non_null(expected);

    amxc_ts_t* actual = amxd_object_get_value(amxc_ts_t, object, key, NULL);
    assert_non_null(actual);

    _assert_int_equal(amxc_ts_compare(actual, expected), cmp, file, line);

    free(actual);
}

/**
 * @brief asserts object timestamp parameter
 * @param object The data model object containing the timestamp parameter.
 * @param key The name of the timestamp parameter
 * @param expected Expected timestamp value to compare to
 * @param cmp  0 if the actual timestamp value is expected to be equal,
 *            -1 if the actual timestamp value is expected to be before,
 *             1 if the actual timestamp value is expected to be after the expected timestamp.
 */
#define assert_object_timestamp_cmp(object, key, expected, cmp) _assert_object_timestamp_cmp(object, key, expected, cmp, __FILE__, __LINE__)


static void _assert_object_cstring_equal(amxd_object_t* object, const char* key, const char* expected, const char* file, int line) {
    assert_non_null(object);
    assert_non_null(key);
    assert_string_not_equal(key, "");

    char* actual = amxd_object_get_value(cstring_t, object, key, NULL);
    assert_non_null(actual);

    _assert_string_equal(actual, expected, file, line);

    free(actual);
}

/**
 * @brief asserts object string parameter value
 * @param object The data model object containing the string parameter.
 * @param key The name of the string parameter
 * @param expected Expected string value
 */
#define assert_object_cstring_equal(object, key, expected) _assert_object_cstring_equal(object, key, expected, __FILE__, __LINE__)


typedef struct stats_values {
    uint64_t txBytes;
    uint64_t rxBytes;
    uint64_t txPackets;
    uint64_t rxPackets;
} stats_values_t;

static void _assert_object_uint64_t_equal(amxd_object_t* object, const char* key, uint64_t expected, const char* file, int line) {
    assert_non_null(object);
    assert_non_null(key);
    assert_string_not_equal(key, "");

    amxd_status_t status = amxd_status_unknown_error;
    uint64_t actual = amxd_object_get_value(uint64_t, object, key, &status);
    if(status != amxd_status_ok) {
        print_error("ERROR: failed to read %s: %s\n",
                    key, amxd_status_string(status));
        _fail(file, line);
    }
    if(actual != expected) {
        print_error("ERROR: %s:%lu != %lu\n",
                    key, actual, expected);
        _fail(file, line);
    }
}

/**
 * @brief asserts object uint64 parameter value
 * @param object The data model object containing the uint64 parameter.
 * @param key The name of the parameter
 * @param expected Expected value
 */
#define assert_object_uint64_t_equal(object, key, expected) _assert_object_uint64_t_equal(object, key, expected, __FILE__, __LINE__)

static void _assert_stats(amxd_object_t* stats, const stats_values_t* expected, const char* file, int line) {
    assert_non_null(stats);

    _assert_object_uint64_t_equal(stats, "BytesSent", expected->txBytes, file, line);
    _assert_object_uint64_t_equal(stats, "BytesReceived", expected->rxBytes, file, line);
    _assert_object_uint64_t_equal(stats, "PacketsSent", expected->txPackets, file, line);
    _assert_object_uint64_t_equal(stats, "PacketsReceived", expected->rxPackets, file, line);
}

/**
 * @brief asserts stats values
 * @param stats The data model .Stats object created by the stats mib.
 * @param key The name of the parameter
 * @param expected Struct with the expected statistics values.
 */
#define assert_stats(stats, expected) _assert_stats(stats, expected, __FILE__, __LINE__)


static void s_fill_arg_values(amxc_var_t* var,
                              const stats_values_t* values,
                              const stats_values_t* exclude) {
    assert_non_null(var);
    assert_non_null(values);
    assert_non_null(exclude);

    if(exclude->rxBytes == 0) {
        amxc_var_add_key(uint64_t, var, "BytesSent", values->txBytes);
    }
    if(exclude->txBytes == 0) {
        amxc_var_add_key(uint64_t, var, "BytesReceived", values->rxBytes);
    }
    if(exclude->rxPackets == 0) {
        amxc_var_add_key(uint64_t, var, "PacketsSent", values->txPackets);
    }
    if(exclude->txPackets == 0) {
        amxc_var_add_key(uint64_t, var, "PacketsReceived", values->rxPackets);
    }
}

static amxd_status_t invoke_setStatistics(amxd_object_t* stats,
                                          amxc_var_t* args,
                                          amxc_var_t* res,
                                          bool expected_res) {
    amxc_var_t args_copy;

    assert_non_null(stats);
    assert_non_null(args);
    assert_non_null(res);

    // Keep a copy for printing, as the data is consumed by amxd_object_invoke_function
    amxc_var_init(&args_copy);
    amxc_var_copy(&args_copy, args);

    amxd_status_t status = amxd_object_invoke_function(stats, "setStatistics", args, res);
    if(amxc_var_dyncast(bool, res) != expected_res) {
        print_error("Call to setStatistics failed: %s(%d)\n", amxd_status_string(status), status);
        print_error("args:\n");
        amxc_var_dump(&args_copy, STDERR_FILENO);
        print_error("res:\n");
        amxc_var_dump(res, STDERR_FILENO);
        assert_true(amxc_var_dyncast(bool, res) == expected_res);
    }

    amxc_var_clean(&args_copy);
    return status;
}

static amxd_status_t call_setStatistics(amxd_object_t* stats,
                                        const char* origin,
                                        const stats_values_t* values,
                                        const stats_values_t* exclude,
                                        bool expected_res) {
    static const stats_values_t exclude_none = { 0 };

    amxc_var_t args;
    amxc_var_t* value_args = NULL;
    amxc_var_t res;
    amxd_status_t status;

    amxc_var_init(&args);
    amxc_var_init(&res);

    if(values == NULL) {
        assert_null(exclude);
    } else if(exclude == NULL) {
        exclude = &exclude_none;
    }

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    if(origin != NULL) {
        amxc_var_add_key(cstring_t, &args, ARG_ORIGIN, origin);
    }
    if(values != NULL) {
        value_args = amxc_var_add_new_key(&args, ARG_VALUES);
        amxc_var_set_type(value_args, AMXC_VAR_ID_HTABLE);
        s_fill_arg_values(value_args, values, exclude);
    }

    status = invoke_setStatistics(stats, &args, &res, expected_res);

    amxc_var_clean(&args);
    amxc_var_clean(&res);

    return status;
}

static amxd_status_t set_origin(amxd_object_t* stats,
                                const char* origin) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;

    status = amxd_trans_init(&trans);
    when_failed(status, exit);

    status = amxd_trans_select_object(&trans, stats);
    when_failed(status, exit);

    status = amxd_trans_set_cstring_t(&trans, PARAM_ORIGIN, origin);
    when_failed(status, exit);

    status = amxd_trans_apply(&trans, amxd_object_get_dm(stats));

exit:
    if(status != amxd_status_ok) {
        printf("set_origin(%s) -> %s (%d):\n", origin, amxd_status_string(status), status);
        fflush(stdout);
        amxd_trans_dump(&trans, STDOUT_FILENO, false);
        fflush(stdout);
    }

    amxd_trans_clean(&trans);
    return status;
}

static amxd_status_t invoke_setStatisticsError(amxd_object_t* stats,
                                               amxc_var_t* args,
                                               amxc_var_t* res,
                                               bool expected_res) {
    amxc_var_t args_copy;

    assert_non_null(stats);
    assert_non_null(args);
    assert_non_null(res);

    // Keep a copy for printing, as the data is consumed by amxd_object_invoke_function
    amxc_var_init(&args_copy);
    amxc_var_copy(&args_copy, args);

    amxd_status_t status = amxd_object_invoke_function(stats, "setStatisticsError", args, res);
    if(amxc_var_dyncast(bool, res) != expected_res) {
        print_error("Call to setStatisticsError failed: %s(%d)\n", amxd_status_string(status), status);
        print_error("args:\n");
        amxc_var_dump(&args_copy, STDERR_FILENO);
        print_error("res:\n");
        amxc_var_dump(res, STDERR_FILENO);
        assert_true(amxc_var_dyncast(bool, res) == expected_res);
    }

    amxc_var_clean(&args_copy);
    return status;
}

static amxd_status_t call_setStatisticsError(amxd_object_t* stats,
                                             const char* origin,
                                             bool expected_res) {
    amxc_var_t args;
    amxc_var_t res;
    amxd_status_t status;

    amxc_var_init(&args);
    amxc_var_init(&res);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    if(origin != NULL) {
        amxc_var_add_key(cstring_t, &args, ARG_ORIGIN, origin);
    }

    status = invoke_setStatisticsError(stats, &args, &res, expected_res);

    amxc_var_clean(&args);
    amxc_var_clean(&res);

    return status;
}


void test_setStatistics_update(AMXB_UNUSED void** state) {
    stats_values_t expected = { 0 };
    stats_values_t set = { 0 };
    amxd_object_t* stats = amxd_object_get_child(dev_obj, "Stats");
    amxc_ts_t default_ts;
    amxc_ts_t ts0 = { .sec = 1 };
    amxc_ts_t ts1 = { .sec = 2 };
    amxc_ts_t ts2 = { .sec = 3 };

    // timestamp sanity checks
    s_init_timestamp_default(&default_ts);
    assert_memory_not_equal(&default_ts, &ts0, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts1, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts2, sizeof(amxc_ts_t));

    // Handle initial startup events
    handle_events();

    // GIVEN an initial Stats object
    assert_non_null(stats);
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, "");
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // WHEN setting the origin
    assert_int_equal(set_origin(stats, ORIGIN_WIFI),
                     amxd_status_ok);
    handle_events();

    // The initial stats are reset
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // WHEN setting new statistics
    set = (stats_values_t) { 2, 3, 4, 5 };
    will_return(__wrap_amxc_ts_now, &ts0);
    assert_int_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, NULL, true),
                     amxd_status_ok);

    // THEN parameters got changed
    expected = (stats_values_t) { 2, 3, 4, 5 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts0, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);


    // GIVEN new statistics
    set = (stats_values_t) { 3, 5, 8, 12 };
    will_return(__wrap_amxc_ts_now, &ts1);
    assert_int_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, NULL, true),
                     amxd_status_ok);

    // THEN parameters got updated
    expected = (stats_values_t) { 3, 5, 8, 12 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts1, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);
}

void test_setStatistics_change_origin(AMXB_UNUSED void** state) {
    stats_values_t expected = { 0 };
    stats_values_t set = { 0 };
    amxd_object_t* stats = amxd_object_get_child(dev_obj, "Stats");
    amxc_ts_t default_ts;
    amxc_ts_t ts0 = { .sec = 1 };
    amxc_ts_t ts1 = { .sec = 2 };
    amxc_ts_t ts2 = { .sec = 3 };
    amxc_ts_t ts3 = { .sec = 4 };

    // timestamp sanity checks
    s_init_timestamp_default(&default_ts);
    assert_memory_not_equal(&default_ts, &ts0, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts1, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts2, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts3, sizeof(amxc_ts_t));

    // Handle initial startup events
    handle_events();

    // GIVEN an initial Stats object
    assert_non_null(stats);
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, "");
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);

    // WHEN setting the origin
    assert_int_equal(set_origin(stats, ORIGIN_WIFI),
                     amxd_status_ok);
    handle_events();

    // The initial stats are reset
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // WHEN setting new statistics
    set = (stats_values_t) { 2, 3, 4, 5 };
    will_return(__wrap_amxc_ts_now, &ts0);
    assert_int_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, NULL, true),
                     amxd_status_ok);

    // THEN parameters got changed
    expected = (stats_values_t) { 2, 3, 4, 5 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts0, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // WHEN changing the origin
    assert_int_equal(set_origin(stats, ORIGIN_ETH),
                     amxd_status_ok);
    handle_events();

    // The initial stats are reset
    expected = (stats_values_t) { 0, 0, 0, 0 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // GIVEN new statistics
    set = (stats_values_t) { 3, 5, 8, 12 };
    will_return(__wrap_amxc_ts_now, &ts1);
    assert_int_equal(call_setStatistics(stats, ORIGIN_ETH, &set, NULL, true),
                     amxd_status_ok);

    // THEN parameters got updated to the differences
    expected = (stats_values_t) { 3, 5, 8, 12 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts1, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts1, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // AND statistics from the old origin are no longer accepted
    set = (stats_values_t) { 3, 6, 9, 14 };
    assert_int_not_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, NULL, false),
                         amxd_status_ok);
}

void test_setStatistics_change_origin_late_event(AMXB_UNUSED void** state) {
    stats_values_t expected = { 0 };
    stats_values_t set = { 0 };
    amxd_object_t* stats = amxd_object_get_child(dev_obj, "Stats");
    amxc_ts_t default_ts;
    amxc_ts_t ts0 = { .sec = 1 };
    amxc_ts_t ts1 = { .sec = 2 };
    amxc_ts_t ts2 = { .sec = 3 };
    amxc_ts_t ts3 = { .sec = 4 };

    // timestamp sanity checks
    s_init_timestamp_default(&default_ts);
    assert_memory_not_equal(&default_ts, &ts0, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts1, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts2, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts3, sizeof(amxc_ts_t));

    // Handle initial startup events
    handle_events();

    // GIVEN an initial Stats object
    assert_non_null(stats);
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, "");
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);

    // WHEN setting the origin
    assert_int_equal(set_origin(stats, ORIGIN_WIFI),
                     amxd_status_ok);
    handle_events();

    // The initial stats are reset
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // WHEN setting new statistics
    set = (stats_values_t) { 2, 3, 4, 5 };
    will_return(__wrap_amxc_ts_now, &ts0);
    assert_int_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, NULL, true),
                     amxd_status_ok);

    // THEN parameters got changed
    expected = (stats_values_t) { 2, 3, 4, 5 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts0, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // WHEN changing the origin
    assert_int_equal(set_origin(stats, ORIGIN_ETH),
                     amxd_status_ok);

    // GIVEN new statistics
    set = (stats_values_t) { 3, 5, 8, 12 };
    will_return(__wrap_amxc_ts_now, &ts1);
    assert_int_equal(call_setStatistics(stats, ORIGIN_ETH, &set, NULL, true),
                     amxd_status_ok);

    // THEN parameters got updated
    expected = (stats_values_t) { 3, 5, 8, 12 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts1, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts1, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // AND statistics from the old origin are no longer accepted
    set = (stats_values_t) { 3, 6, 9, 14 };
    assert_int_not_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, NULL, false),
                         amxd_status_ok);

    // WHEN the internal event loop is a bit late
    handle_events();

    // The initial stats are not destroyed
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts1, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts1, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);
}

void test_setStatistics_partial(AMXB_UNUSED void** state) {
    stats_values_t expected = { 0 };
    stats_values_t set = { 0 };
    stats_values_t mask = { 0 };
    amxd_object_t* stats = amxd_object_get_child(dev_obj, "Stats");
    amxc_ts_t default_ts;
    amxc_ts_t ts0 = { .sec = 1 };
    amxc_ts_t ts1 = { .sec = 2 };
    amxc_ts_t ts2 = { .sec = 3 };
    amxc_ts_t ts3 = { .sec = 4 };

    // timestamp sanity checks
    s_init_timestamp_default(&default_ts);
    assert_memory_not_equal(&default_ts, &ts0, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts1, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts2, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts3, sizeof(amxc_ts_t));

    // Handle initial startup events
    handle_events();

    // GIVEN an initial Stats object
    assert_non_null(stats);
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, "");
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // WHEN setting the origin
    assert_int_equal(set_origin(stats, ORIGIN_WIFI),
                     amxd_status_ok);
    handle_events();
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);

    // WHEN setting new statistics
    set = (stats_values_t) { 2, 3, 4, 5 };
    mask = (stats_values_t) { .rxBytes = 1, .txBytes = 1 };
    will_return(__wrap_amxc_ts_now, &ts0);
    assert_int_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, &mask, true),
                     amxd_status_ok);

    // THEN parameters got changed
    expected = (stats_values_t) { 0, 0, 4, 5 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts0, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // WHEN doing another partial update
    set = (stats_values_t) { 3, 5, 8, 12 };
    mask = (stats_values_t) { .rxBytes = 1, .rxPackets = 1 };
    will_return(__wrap_amxc_ts_now, &ts1);
    assert_int_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, &mask, true),
                     amxd_status_ok);

    // THEN parameters got updated
    expected = (stats_values_t) { 0, 5, 4, 12 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts1, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // WHEN changing the origin
    assert_int_equal(set_origin(stats, ORIGIN_ETH),
                     amxd_status_ok);
    handle_events();

    // The initial stats are reset
    expected = (stats_values_t) { 0, 0, 0, 0 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // GIVEN new statistics
    set = (stats_values_t) { 3, 5, 8, 12 };
    mask = (stats_values_t) { .txPackets = 1, .rxPackets = 1 };
    will_return(__wrap_amxc_ts_now, &ts2);
    assert_int_equal(call_setStatistics(stats, ORIGIN_ETH, &set, &mask, true),
                     amxd_status_ok);

    // THEN parameters got updated
    expected = (stats_values_t) { 3, 5, 0, 0 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts2, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts2, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // AND statistics from the old origin are no longer accepted
    set = (stats_values_t) { 3, 6, 9, 14 };
    will_return_maybe(__wrap_amxc_ts_now, &ts3);
    assert_int_not_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, &mask, false),
                         amxd_status_ok);

    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts2, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts2, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);
}

void test_setStatistics_partial_late_event(AMXB_UNUSED void** state) {
    stats_values_t expected = { 0 };
    stats_values_t set = { 0 };
    stats_values_t mask = { 0 };
    amxd_object_t* stats = amxd_object_get_child(dev_obj, "Stats");
    amxc_ts_t default_ts;
    amxc_ts_t ts0 = { .sec = 1 };
    amxc_ts_t ts1 = { .sec = 2 };
    amxc_ts_t ts2 = { .sec = 3 };
    amxc_ts_t ts3 = { .sec = 4 };

    // timestamp sanity checks
    s_init_timestamp_default(&default_ts);
    assert_memory_not_equal(&default_ts, &ts0, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts1, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts2, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts3, sizeof(amxc_ts_t));

    // Handle initial startup events
    handle_events();

    // GIVEN an initial Stats object
    assert_non_null(stats);
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, "");
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // WHEN setting the origin
    assert_int_equal(set_origin(stats, ORIGIN_WIFI),
                     amxd_status_ok);
    handle_events();
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);

    // WHEN setting new statistics
    set = (stats_values_t) { 2, 3, 4, 5 };
    mask = (stats_values_t) { .rxBytes = 1, .txBytes = 1 };
    will_return(__wrap_amxc_ts_now, &ts0);
    assert_int_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, &mask, true),
                     amxd_status_ok);

    // THEN parameters got changed
    expected = (stats_values_t) { 0, 0, 4, 5 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts0, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // WHEN doing another partial update
    set = (stats_values_t) { 3, 5, 8, 12 };
    mask = (stats_values_t) { .rxBytes = 1, .rxPackets = 1 };
    will_return(__wrap_amxc_ts_now, &ts1);
    assert_int_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, &mask, true),
                     amxd_status_ok);

    // THEN parameters got updated
    expected = (stats_values_t) { 0, 5, 4, 12 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts1, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // WHEN changing the origin, but event processing is a bit late
    assert_int_equal(set_origin(stats, ORIGIN_ETH),
                     amxd_status_ok);

    // GIVEN new statistics
    set = (stats_values_t) { 3, 5, 8, 12 };
    mask = (stats_values_t) { .txPackets = 1, .rxPackets = 1 };
    will_return(__wrap_amxc_ts_now, &ts2);
    assert_int_equal(call_setStatistics(stats, ORIGIN_ETH, &set, &mask, true),
                     amxd_status_ok);

    // THEN parameters got updated
    expected = (stats_values_t) { 3, 5, 0, 0 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts2, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts2, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // AND statistics from the old origin are no longer accepted
    set = (stats_values_t) { 3, 6, 9, 14 };
    will_return_maybe(__wrap_amxc_ts_now, &ts3);
    assert_int_not_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, &mask, false),
                         amxd_status_ok);

    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts2, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts2, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // WHEN the event loop catches up
    handle_events();

    // THEN the stats are not destroyed
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts2, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts2, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);
}

void test_setStatistics_int_types(AMXB_UNUSED void** state) {
    amxd_object_t* stats = amxd_object_get_child(dev_obj, "Stats");
    stats_values_t expected = { 0 };
    amxc_var_t args;
    amxc_var_t* value_args = NULL;
    amxc_var_t res;
    amxc_ts_t default_ts;
    amxc_ts_t ts0 = { .sec = 1 };

    amxc_var_init(&args);
    amxc_var_init(&res);

    // timestamp sanity checks
    s_init_timestamp_default(&default_ts);
    assert_memory_not_equal(&default_ts, &ts0, sizeof(amxc_ts_t));

    // GIVEN a good origin and everything ready to set stats
    assert_int_equal(set_origin(stats, ORIGIN_ETH),
                     amxd_status_ok);
    handle_events();

    assert_non_null(stats);
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);


    // WHEN setting the statistics using various integer types with valid values
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, ARG_ORIGIN, ORIGIN_ETH);
    value_args = amxc_var_add_new_key(&args, ARG_VALUES);
    amxc_var_set_type(value_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(int8_t, value_args, "BytesSent", 42);
    amxc_var_add_key(int64_t, value_args, "BytesReceived", INT64_MAX);
    amxc_var_add_key(uint8_t, value_args, "PacketsSent", 128);
    amxc_var_add_key(cstring_t, value_args, "PacketsReceived", "9223372036854775807");
    will_return(__wrap_amxc_ts_now, &ts0);
    assert_int_equal(invoke_setStatistics(stats, &args, &res, true),
                     amxd_status_ok);

    // All values were set to the correct uint64 value
    expected = (stats_values_t) { 42, INT64_MAX, 128, INT64_MAX };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts0, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // cleanup
    amxc_var_clean(&args);
    amxc_var_clean(&res);
}

void test_setStatistics_error_bad_arguments(AMXB_UNUSED void** state) {
    amxd_object_t* stats = amxd_object_get_child(dev_obj, "Stats");
    stats_values_t set = { 0 };
    amxc_ts_t ts = { .sec = 1 };

    will_return_maybe(__wrap_amxc_ts_now, &ts);

    assert_int_equal(set_origin(stats, ORIGIN_WIFI),
                     amxd_status_ok);
    handle_events();

    /* Incorrect origin */
    assert_int_not_equal(call_setStatistics(stats, ORIGIN_ETH, &set, NULL, false),
                         amxd_status_ok);
    /* Missing values */
    assert_int_not_equal(call_setStatistics(stats, ORIGIN_WIFI, NULL, NULL, false),
                         amxd_status_ok);
    /* Missing or bad origin */
    assert_int_not_equal(call_setStatistics(stats, NULL, &set, NULL, false),
                         amxd_status_ok);
    assert_int_not_equal(call_setStatistics(stats, "", &set, NULL, false),
                         amxd_status_ok);
    assert_int_not_equal(call_setStatistics(stats, "test", &set, NULL, false),
                         amxd_status_ok);
}

void test_setStatistics_error_bad_stats(AMXB_UNUSED void** state) {
    amxd_object_t* stats = amxd_object_get_child(dev_obj, "Stats");
    stats_values_t expected = { 0 };
    amxc_var_t args;
    amxc_var_t* value_args = NULL;
    amxc_var_t res;
    amxc_ts_t default_ts;
    amxc_ts_t ts = { .sec = 1 };

    amxc_var_init(&args);
    amxc_var_init(&res);

    // timestamp sanity checks
    s_init_timestamp_default(&default_ts);
    assert_memory_not_equal(&default_ts, &ts, sizeof(amxc_ts_t));

    will_return_maybe(__wrap_amxc_ts_now, &ts);

    // GIVEN an initially set up stats mib
    assert_int_equal(set_origin(stats, ORIGIN_ETH),
                     amxd_status_ok);
    handle_events();
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // WHEN trying to pass all kinds of bad statistics
    // Unknown statistics name
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, ARG_ORIGIN, ORIGIN_ETH);
    value_args = amxc_var_add_new_key(&args, ARG_VALUES);
    amxc_var_set_type(value_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint64_t, value_args, "SomeOtherStatistic", 42);
    assert_int_not_equal(invoke_setStatistics(stats, &args, &res, false),
                         amxd_status_ok);

    // Existing Non-statistics name
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, ARG_ORIGIN, ORIGIN_ETH);
    value_args = amxc_var_add_new_key(&args, ARG_VALUES);
    amxc_var_set_type(value_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint64_t, value_args, PARAM_INITIAL_TIME, 42);
    assert_int_not_equal(invoke_setStatistics(stats, &args, &res, false),
                         amxd_status_ok);

    // Bad format
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, ARG_ORIGIN, ORIGIN_ETH);
    value_args = amxc_var_add_new_key(&args, ARG_VALUES);
    amxc_var_set_type(value_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(csv_string_t, value_args, "BytesSent", "42,");
    assert_int_not_equal(invoke_setStatistics(stats, &args, &res, false),
                         amxd_status_ok);

    // Negative value
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, ARG_ORIGIN, ORIGIN_ETH);
    value_args = amxc_var_add_new_key(&args, ARG_VALUES);
    amxc_var_set_type(value_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(int64_t, value_args, "BytesSent", -42);
    assert_int_not_equal(invoke_setStatistics(stats, &args, &res, false),
                         amxd_status_ok);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, ARG_ORIGIN, ORIGIN_ETH);
    value_args = amxc_var_add_new_key(&args, ARG_VALUES);
    amxc_var_set_type(value_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, value_args, "BytesSent", "-42");
    assert_int_not_equal(invoke_setStatistics(stats, &args, &res, false),
                         amxd_status_ok);

    // Value out of bounds
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, ARG_ORIGIN, ORIGIN_ETH);
    value_args = amxc_var_add_new_key(&args, ARG_VALUES);
    amxc_var_set_type(value_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, value_args, "BytesSent", "99999999999999999999999999999999");
    assert_int_not_equal(invoke_setStatistics(stats, &args, &res, false),
                         amxd_status_ok);

    // THEN the data in the data model remains unchanged and valid
    handle_events();
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // cleanup
    amxc_var_clean(&args);
    amxc_var_clean(&res);
}

void test_setStatisticsError(AMXB_UNUSED void** state) {
    stats_values_t expected = { 0 };
    stats_values_t set = { 0 };
    amxd_object_t* stats = amxd_object_get_child(dev_obj, "Stats");
    amxc_ts_t default_ts;
    amxc_ts_t ts0 = { .sec = 1 };
    amxc_ts_t ts1 = { .sec = 2 };
    amxc_ts_t ts2 = { .sec = 3 };
    amxc_ts_t ts3 = { .sec = 4 };

    // timestamp sanity checks
    s_init_timestamp_default(&default_ts);
    assert_memory_not_equal(&default_ts, &ts0, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts1, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts2, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts3, sizeof(amxc_ts_t));

    // Handle initial startup events
    handle_events();

    // GIVEN an initial Stats object
    assert_non_null(stats);
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, "");
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);

    // WHEN setting the origin
    assert_int_equal(set_origin(stats, ORIGIN_WIFI),
                     amxd_status_ok);
    handle_events();

    // The initial stats are reset
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // WHEN setting the error state
    assert_int_equal(call_setStatisticsError(stats, ORIGIN_WIFI, true),
                     amxd_status_ok);

    // THEN only the status parameter was changed
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_ERROR);

    // WHEN setting new statistics
    set = (stats_values_t) { 2, 3, 4, 5 };
    will_return(__wrap_amxc_ts_now, &ts0);
    assert_int_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, NULL, true),
                     amxd_status_ok);

    // THEN parameters got changed
    expected = (stats_values_t) { 2, 3, 4, 5 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts0, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);


    // WHEN setting the error state
    assert_int_equal(call_setStatisticsError(stats, ORIGIN_WIFI, true),
                     amxd_status_ok);

    // THEN only the status parameter was changed
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts0, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_ERROR);

    // WHEN changing the origin
    assert_int_equal(set_origin(stats, ORIGIN_ETH),
                     amxd_status_ok);
    handle_events();

    // The initial stats are reset
    expected = (stats_values_t) { 0, 0, 0, 0 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // WHEN setting the error state
    assert_int_equal(call_setStatisticsError(stats, ORIGIN_ETH, true),
                     amxd_status_ok);

    // THEN only the status parameter was changed
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_ERROR);

    // GIVEN new statistics
    set = (stats_values_t) { 3, 5, 8, 12 };
    will_return(__wrap_amxc_ts_now, &ts1);
    assert_int_equal(call_setStatistics(stats, ORIGIN_ETH, &set, NULL, true),
                     amxd_status_ok);

    // THEN parameters got updated to the differences
    expected = (stats_values_t) { 3, 5, 8, 12 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts1, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts1, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // AND statistics from the old origin are no longer accepted
    set = (stats_values_t) { 3, 6, 9, 14 };
    assert_int_not_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, NULL, false),
                         amxd_status_ok);

    assert_int_not_equal(call_setStatisticsError(stats, ORIGIN_WIFI, false),
                         amxd_status_ok);

    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts1, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts1, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);
}

void test_setStatisticsError_late_event(AMXB_UNUSED void** state) {
    stats_values_t expected = { 0 };
    stats_values_t set = { 0 };
    amxd_object_t* stats = amxd_object_get_child(dev_obj, "Stats");
    amxc_ts_t default_ts;
    amxc_ts_t ts0 = { .sec = 1 };
    amxc_ts_t ts1 = { .sec = 2 };

    // timestamp sanity checks
    s_init_timestamp_default(&default_ts);
    assert_memory_not_equal(&default_ts, &ts0, sizeof(amxc_ts_t));
    assert_memory_not_equal(&default_ts, &ts1, sizeof(amxc_ts_t));

    // Handle initial startup events
    handle_events();

    // GIVEN an initial Stats object
    assert_non_null(stats);
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, "");
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);

    // WHEN setting the origin
    assert_int_equal(set_origin(stats, ORIGIN_WIFI),
                     amxd_status_ok);
    handle_events();

    // The initial stats are reset
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_INIT);

    // WHEN setting the error state
    assert_int_equal(call_setStatisticsError(stats, ORIGIN_WIFI, true),
                     amxd_status_ok);

    // THEN only the status parameter was changed
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_ERROR);

    // WHEN setting new statistics
    set = (stats_values_t) { 2, 3, 4, 5 };
    will_return(__wrap_amxc_ts_now, &ts0);
    assert_int_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, NULL, true),
                     amxd_status_ok);

    // THEN parameters got changed
    expected = (stats_values_t) { 2, 3, 4, 5 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts0, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);


    // WHEN setting the error state
    assert_int_equal(call_setStatisticsError(stats, ORIGIN_WIFI, true),
                     amxd_status_ok);

    // THEN only the status parameter was changed
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_WIFI);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts0, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts0, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_ERROR);

    // WHEN changing the origin
    assert_int_equal(set_origin(stats, ORIGIN_ETH),
                     amxd_status_ok);

    // AND immediately setting the error state
    assert_int_equal(call_setStatisticsError(stats, ORIGIN_ETH, true),
                     amxd_status_ok);

    // The initial stats are reset
    expected = (stats_values_t) { 0, 0, 0, 0 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_ERROR);


    // WHEN the event loop catches up
    handle_events();

    // THEN the stats are not destroyed
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &default_ts, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &default_ts, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_ERROR);

    // GIVEN new statistics
    set = (stats_values_t) { 3, 5, 8, 12 };
    will_return(__wrap_amxc_ts_now, &ts1);
    assert_int_equal(call_setStatistics(stats, ORIGIN_ETH, &set, NULL, true),
                     amxd_status_ok);

    // THEN parameters got updated to the differences
    expected = (stats_values_t) { 3, 5, 8, 12 };
    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts1, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts1, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);

    // AND statistics from the old origin are no longer accepted
    set = (stats_values_t) { 3, 6, 9, 14 };
    assert_int_not_equal(call_setStatistics(stats, ORIGIN_WIFI, &set, NULL, false),
                         amxd_status_ok);

    assert_int_not_equal(call_setStatisticsError(stats, ORIGIN_WIFI, false),
                         amxd_status_ok);

    assert_stats(stats, &expected);
    assert_object_cstring_equal(stats, PARAM_ORIGIN, ORIGIN_ETH);
    assert_object_timestamp_cmp(stats, PARAM_INITIAL_TIME, &ts1, 0);
    assert_object_timestamp_cmp(stats, PARAM_LAST_UPDATE, &ts1, 0);
    assert_object_cstring_equal(stats, PARAM_STATUS, STATUS_SUCCESS);
}

void test_setStatisticsError_error_bad_arguments(AMXB_UNUSED void** state) {
    amxd_object_t* stats = amxd_object_get_child(dev_obj, "Stats");
    amxc_ts_t ts = { .sec = 1 };

    will_return_maybe(__wrap_amxc_ts_now, &ts);

    assert_int_equal(set_origin(stats, ORIGIN_WIFI),
                     amxd_status_ok);
    handle_events();

    /* Incorrect origin */
    assert_int_not_equal(call_setStatisticsError(stats, ORIGIN_ETH, false),
                         amxd_status_ok);
    /* Missing values */
    assert_int_not_equal(call_setStatisticsError(stats, NULL, false),
                         amxd_status_ok);
    assert_int_not_equal(call_setStatisticsError(stats, "", false),
                         amxd_status_ok);
}
