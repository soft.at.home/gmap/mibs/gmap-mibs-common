/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_wan_access.h"
#include "../common/dummy.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_variant.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>

#include <gmap/gmap.h>

amxd_status_t _block(amxd_object_t* object,
                     amxd_function_t* func,
                     amxc_var_t* args,
                     amxc_var_t* ret);

amxd_status_t _unblock(amxd_object_t* object,
                       amxd_function_t* func,
                       amxc_var_t* args,
                       amxc_var_t* ret);

/**********************************************************
* Macro definitions
**********************************************************/

#define MIB_WANACCESS_FIELD_WANACCESS "WANAccess"
#define MIB_WANACCESS_FIELD_BLOCKED_REASONS "BlockedReasons"
#define MIB_WANACCESS_FIELD_BLOCKED_TAG "wan-blocked"

/**********************************************************
* Variable declarations
**********************************************************/
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxd_object_t* dev_obj = NULL;
static amxd_object_t* wan_acc_obj = NULL;

static bool load_mib_module() {
    amxo_parser_t* parser = test_get_parser();
    amxd_dm_t* dm = test_get_dm();
    dev_obj = amxd_object_findf(&dm->object, "Devices.Device.temp.");

    assert_int_equal(amxo_resolver_ftab_add(parser, "block", AMXO_FUNC(_block)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "unblock", AMXO_FUNC(_unblock)), 0);

    amxo_parser_scan_mib_dir(parser, "../../mibs");
    amxo_parser_load_mib(parser, dm, "wanAccess");

    amxo_parser_apply_mib(parser, dev_obj, "wanAccess");

    wan_acc_obj = amxd_object_get(dev_obj, "WANAccess");

    return true;
}

int test_wan_access_setup(AMXB_UNUSED void** state) {
    amxc_var_t* var = NULL;
    amxo_parser_t* parser = NULL;

    assert_int_equal(test_register_dummy(), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    gmap_client_init(bus_ctx);

    // parser and dm are created in dummy connect
    parser = test_get_parser();

    assert_int_equal(amxo_resolver_ftab_add(parser, "load", AMXO_FUNC(_load)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "get", AMXO_FUNC(_get)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setTag", AMXO_FUNC(_setTag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "clearTag", AMXO_FUNC(_clearTag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setActive", AMXO_FUNC(_setActive)), 0);

    // do not load import so files
    var = amxo_parser_claim_config(parser, "odl-import");
    amxc_var_set(bool, var, false);

    assert_int_equal(test_load_dummy_remote("../common/dummy_gmap.odl"), 0);

    load_mib_module();

    return amxd_status_ok;
}

int test_wan_access_teardown(AMXB_UNUSED void** state) {
    amxo_parser_remove_mibs(test_get_parser(), dev_obj, NULL);
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);
    assert_int_equal(test_unregister_dummy(), 0);

    return amxd_status_ok;
}

void test_block(AMXB_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    char* reason = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "reason", "Global");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "block",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    reason = amxd_object_get_value(cstring_t, wan_acc_obj, MIB_WANACCESS_FIELD_BLOCKED_REASONS, NULL);
    assert_string_equal(reason, "Global");
    free(reason);

    reason = amxd_object_get_value(cstring_t, dev_obj, "Tags", NULL);
    assert_string_equal(reason, "lan physical mac useragent ipv4 ipv6 wan-blocked");
    free(reason);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_unblock(AMXB_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    char* reason = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "reason", "Global");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "unblock",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    reason = amxd_object_get_value(cstring_t, wan_acc_obj, MIB_WANACCESS_FIELD_BLOCKED_REASONS, NULL);
    assert_string_equal(reason, "");
    free(reason);

    reason = amxd_object_get_value(cstring_t, dev_obj, "Tags", NULL);
    assert_string_equal(reason, "lan physical mac useragent ipv4 ipv6");
    free(reason);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_block_multiple(AMXB_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    char* reason = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    AMXB_UNUSED amxc_var_t* var = amxc_var_add_key(cstring_t, &args, "reason", "Global");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "block",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_add_key(cstring_t, &args, "reason", "MaxSurfTime");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "block",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    reason = amxd_object_get_value(cstring_t, wan_acc_obj, MIB_WANACCESS_FIELD_BLOCKED_REASONS, NULL);
    assert_string_equal(reason, "Global MaxSurfTime");
    free(reason);

    reason = amxd_object_get_value(cstring_t, dev_obj, "Tags", NULL);
    assert_string_equal(reason, "lan physical mac useragent ipv4 ipv6 wan-blocked");
    free(reason);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_unblock_multiple(AMXB_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    char* reason = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "reason", "Global");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "unblock",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    reason = amxd_object_get_value(cstring_t, wan_acc_obj, MIB_WANACCESS_FIELD_BLOCKED_REASONS, NULL);
    assert_string_equal(reason, "MaxSurfTime");
    free(reason);

    reason = amxd_object_get_value(cstring_t, dev_obj, "Tags", NULL);
    assert_string_equal(reason, "lan physical mac useragent ipv4 ipv6 wan-blocked");
    free(reason);

    amxc_var_add_key(cstring_t, &args, "reason", "MaxSurfTime");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "unblock",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    reason = amxd_object_get_value(cstring_t, wan_acc_obj, MIB_WANACCESS_FIELD_BLOCKED_REASONS, NULL);
    assert_string_equal(reason, "");
    free(reason);

    reason = amxd_object_get_value(cstring_t, dev_obj, "Tags", NULL);
    assert_string_equal(reason, "lan physical mac useragent ipv4 ipv6");
    free(reason);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_unblock_all(AMXB_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    char* reason = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "reason", "Global");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "block",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_add_key(cstring_t, &args, "reason", "MaxSurfTime");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "block",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    reason = amxd_object_get_value(cstring_t, dev_obj, "Tags", NULL);
    assert_string_equal(reason, "lan physical mac useragent ipv4 ipv6 wan-blocked");
    free(reason);

    amxc_var_add_key(cstring_t, &args, "reason", "");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "unblock",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    reason = amxd_object_get_value(cstring_t, wan_acc_obj, MIB_WANACCESS_FIELD_BLOCKED_REASONS, NULL);
    assert_string_equal(reason, "");
    free(reason);

    reason = amxd_object_get_value(cstring_t, dev_obj, "Tags", NULL);
    assert_string_equal(reason, "lan physical mac useragent ipv4 ipv6");
    free(reason);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cannot_block_with_wrong_reason(AMXB_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    char* reason = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "reason", "Wrong");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "block",
                                                 &args,
                                                 &ret),
                     amxd_status_invalid_function_argument);

    amxc_var_add_key(cstring_t, &args, "reason", "Glo");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "block",
                                                 &args,
                                                 &ret),
                     amxd_status_invalid_function_argument);

    amxc_var_add_key(cstring_t, &args, "reason", "Global2");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "block",
                                                 &args,
                                                 &ret),
                     amxd_status_invalid_function_argument);

    reason = amxd_object_get_value(cstring_t, wan_acc_obj, MIB_WANACCESS_FIELD_BLOCKED_REASONS, NULL);
    assert_string_equal(reason, "");
    free(reason);

    reason = amxd_object_get_value(cstring_t, dev_obj, "Tags", NULL);
    assert_string_equal(reason, "lan physical mac useragent ipv4 ipv6");
    free(reason);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_cannot_unblock_with_wrong_reason(AMXB_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    char* reason = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "reason", "Global");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "block",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_add_key(cstring_t, &args, "reason", "Wrong");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "unblock",
                                                 &args,
                                                 &ret),
                     amxd_status_invalid_function_argument);

    amxc_var_add_key(cstring_t, &args, "reason", "MaxSurfTime");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "unblock",
                                                 &args,
                                                 &ret),
                     amxd_status_invalid_function_argument);

    reason = amxd_object_get_value(cstring_t, dev_obj, "Tags", NULL);
    assert_string_equal(reason, "lan physical mac useragent ipv4 ipv6 wan-blocked");
    free(reason);

    amxc_var_add_key(cstring_t, &args, "reason", "Global");
    assert_int_equal(amxd_object_invoke_function(wan_acc_obj,
                                                 "unblock",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    reason = amxd_object_get_value(cstring_t, wan_acc_obj, MIB_WANACCESS_FIELD_BLOCKED_REASONS, NULL);
    assert_string_equal(reason, "");
    free(reason);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
