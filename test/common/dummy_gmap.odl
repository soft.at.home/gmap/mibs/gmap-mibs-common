object Devices {
    object Config {
        variant get(%mandatory string module, %mandatory string option);
        bool load(%mandatory string module);
        object global {
            ssv_string BlockedReasons = "MaxSurfTime Global SecurityGateway";
        }
    }
    object Device[] {
        %unique %key string Alias;

        string Key;
    
        /* for bridges: */
        ssv_string Tags;
        
        bool setTag(%in string tag = "", %in string expression = "", %in string traverse = "");

        bool hasTag(%in %mandatory string tag, %in string expression = "", %in string traverse);

        bool clearTag(%in string tag = "", %in string expression = "", %in string traverse = "");

        /**
         * Indicates that the device is currently active or not.
         *
         * This value is computed according to tho following algorithm:
         * The instance in Devices.Device.{i}.Actives with the highest priority is selected
         * (i.e. priority value closest to 1),
         * if multiple instances have the highest priority then it's a AND between all the
         * values.
         * If no Actives instance exists, the Active parameter is considered false.
         *
         * @version 1.0
         */
        %read-only bool Active = false;

        /**
         * Sets a device active or inactive according to given source
         *
         * Adds an entry under Devices.Device.{i}.Actives with given data.
         * In case an entry exists with given source, this entry is updated.
         * Devices.Device.{i}.Active will automatically be re-calculated.
         * 
         * @param active: See Devices.Device.{i}.Actives.{j}.Active
         * @param source: See Devices.Device.{i}.Actives.{j}.Source
         * @param priority: See Devices.Device.{i}.Actives.{j}.Priority
         *
         * @version 1.0
         */
        void setActive(%mandatory bool active = true, %mandatory string source, %mandatory uint32 priority = 100);

        /**
         * Contains the list of available active status.
         *
         * Used to map to the Devices.Device.{i}.Active parameter.
         *
         * @version 1.0
         */
        %read-only object Actives[] {

            /**
             * Source of Active value, this value is also used as key for the instance
             *
             * @version 1.0
             */
            %unique %key string Source;
        
            /**
             * Active value of the device
             *
             * @version 1.0
             */
            %read-only bool Active;
        
            /**
             * The priority of the Active entry for selecting the Active field of the device.
             * Value between 1 - 100. With 1 the highest priority
             *
             * @version 1.0
             */
            %read-only uint32 Priority {
                default 100;
                on action validate call check_range [1,100];
            }
        
            /**
             * Last update timestamp of Active value.
             *
             * @version 1.0
             */
            %volatile %read-only datetime Time;
        }
    }
}

%populate {
    object Devices.Device {
        instance add("temp") {
            parameter Tags = "lan physical mac useragent ipv4 ipv6";
            parameter Key = "temp";
        }
    }
    object Devices.Device {
        instance add("lan") {
            parameter Tags = "interface physical mac useragent ipv4 ipv6";
            parameter Key = "lan";
            parameter Active = 1;
        }
    }
}