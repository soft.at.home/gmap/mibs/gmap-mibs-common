# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.24.7 - 2024-12-03(12:57:41 +0000)

### Other

- [prpl] Crash gmap_server

## Release v0.24.6 - 2024-11-12(12:13:40 +0000)

### Other

- Empty values for ManageableDevice parameters after an upgrade

## Release v0.24.5 - 2024-10-21(13:39:52 +0000)

### Other

- Adding new mdns record does not trigger a lookup

## Release v0.24.4 - 2024-09-25(14:21:35 +0000)

### Other

- [BDD][PrPl] missing MDS services on PRPL (umdns + parser instead of avahi )

## Release v0.24.3 - 2024-07-29(11:09:13 +0000)

### Other

- [gmap][mod-upnp] Add FriendlyName and update Server value

## Release v0.24.2 - 2024-06-29(04:25:00 +0000)

### Other

- [gmap][bridge] Bridge device in gmap doesnt remove old ip address from IP.Interface

## Release v0.24.1 - 2024-06-20(08:30:18 +0000)

### Other

- [gMap][Stats][API] Implement the stats MiB

## Release v0.24.0 - 2024-06-12(08:32:00 +0000)

### Other

- [gmap][mod-sahpairing] Add new 'FirmwareImageStatus' parameter

## Release v0.23.0 - 2024-05-14(08:39:47 +0000)

### Other

- Keep last known IP addresses in the data model

## Release v0.22.1 - 2024-04-19(13:48:11 +0000)

### Other

- [gmap] DeviceCategories are not upgrade persistent

## Release v0.22.0 - 2024-04-03(13:52:49 +0000)

### Other

- [AMX] [GMAP] Set a default value for DeviceCategory

## Release v0.21.1 - 2024-03-19(20:59:00 +0000)

### Other

- [gmap] remove the pcb style location tag in the gmap mibs (on amx)

## Release v0.21.0 - 2024-03-12(14:28:23 +0000)

### Other

- Add support for backup and restore

## Release v0.20.3 - 2024-03-04(11:59:37 +0000)

### Other

- Fix typos in documentation

## Release v0.20.2 - 2024-02-29(15:24:56 +0000)

### Other

- [Prpl] BDD MIB in gMap data model

## Release v0.20.1 - 2024-02-05(14:56:21 +0000)

### Other

- [Prpl] BDD MIB in gMap data model

## Release v0.20.0 - 2024-02-01(14:45:02 +0000)

### Other

- [gmap][mibs][information] add setInformationList function

## Release v0.19.2 - 2024-01-30(09:03:23 +0000)

### Other

- set 'Portable' as default value of 'Mobility' parameter

## Release v0.19.1 - 2024-01-23(12:27:32 +0000)

### Other

- [gmap][mibs][useragent] Clean up the the signal slot for EmbeddedFiltering

## Release v0.19.0 - 2024-01-18(11:36:10 +0000)

### Other

- Let Devices.Device.i.Active.[Source=='mib-ip'].Priority=50

## Release v0.18.1 - 2024-01-16(14:42:33 +0000)

### Changes

- [gmap] Load config odls at startup of gmap-server

## Release v0.18.0 - 2024-01-12(12:18:19 +0000)

### New

- [gmap][mibs] Port priority mib to amx

## Release v0.17.0 - 2024-01-11(09:48:23 +0000)

### Other

- Add Devices.Device.i.Layer1Interface

## Release v0.16.1 - 2024-01-10(10:36:50 +0000)

### Other

- [gmap][useragent] useragent is not catching the EmbeddedFiltering event

## Release v0.16.0 - 2024-01-05(10:16:58 +0000)

### Other

- Load location config on location mib load

## Release v0.15.4 - 2023-12-18(14:30:22 +0000)

### Other

- [gmap][mibs][useragent] the contructor in the mib should be changed to an entry-point

## Release v0.15.3 - 2023-12-15(14:50:59 +0000)

### Other

- [gmap][ethNetDev] MaxBitrateSupported type needs to be changed to signed

## Release v0.15.2 - 2023-12-15(14:24:48 +0000)

### Other

- Added fetchFingerprints api to prpl

## Release v0.15.1 - 2023-12-14(16:41:48 +0000)

### Other

- [gmap][mibs][useragent] the contructor in the mib should be changed to an entry-point

## Release v0.15.0 - 2023-12-08(08:49:30 +0000)

### Other

- [BDD] Create an mdns parser module in AMX

## Release v0.14.0 - 2023-11-13(12:06:32 +0000)

### Other

- Move Locations config to config level and add CustomLocation at device level

## Release v0.13.0 - 2023-11-09(17:29:02 +0000)

### Other

- Add Mobility parameter

## Release v0.12.0 - 2023-10-26(12:05:47 +0000)

### Other

- [amx][gmap] move non-generic functions out of libgmap-client

## Release v0.11.2 - 2023-10-19(10:25:50 +0000)

### Other

- Add multi-instance objects for missing bdd parameters

## Release v0.11.1 - 2023-10-09(11:18:36 +0000)

### Other

- [AMX gMap] UserAgents are not present in Devices.Device DM

## Release v0.11.0 - 2023-09-22(16:18:04 +0000)

### New

- - [DeviceInfo][Location] Allow a user to statically set its Location Configuration

### Other

- [BDD][Gmap Mib] mapping issues detected

## Release v0.10.0 - 2023-08-24(12:33:54 +0000)

### Other

- gMap Homeplug Plugin

## Release v0.9.0 - 2023-08-17(09:54:07 +0000)

### Other

- [gMap][SAHPariring] Add a dedicated sahpairing mib/Client

## Release v0.8.3 - 2023-07-12(12:46:56 +0000)

### Other

- [BDD] BDD parameteres in datamodel should not be read only

## Release v0.8.2 - 2023-07-10(09:06:22 +0000)

### Other

- [gmap][mod-upnp] update the odl so it is not read only

## Release v0.8.1 - 2023-07-03(12:07:19 +0000)

### Other

- [gmap][mibs][useragent] Removing a useragent doesnt send out an event

## Release v0.8.0 - 2023-06-20(06:40:55 +0000)

### Other

- [Useragent][EmbeddedFiltering] Create the new AMX Useragent Embeddedfiltering module

## Release v0.7.1 - 2023-06-15(09:59:01 +0000)

### Other

- [amx][gmap] Fix security problems with long names etc

## Release v0.7.0 - 2023-05-04(07:13:48 +0000)

### Other

- [gmap][UPNPDiscovery] Add UPNP support in gMap

## Release v0.6.0 - 2023-04-21(11:07:38 +0000)

### Other

- It must be possible to populate Gmap with detected UserAgents

## Release v0.5.4 - 2023-04-17(11:41:10 +0000)

### Fixes

- [odl]Remove deprecated odl keywords

## Release v0.5.3 - 2023-02-23(10:02:30 +0000)

### Other

- Adapt to new setActive API with source and priority

## Release v0.5.2 - 2023-02-15(08:30:13 +0000)

### Other

- Rename nemo->netdev and adapt to new 'style' of mib expression

## Release v0.5.1 - 2023-02-08(15:24:38 +0000)

### Other

- [gMap][BDD] Create Information MiB

## Release v0.5.0 - 2023-02-08(11:05:14 +0000)

### Other

- [BDD][gMap] Create BDD MiB

## Release v0.4.3 - 2023-01-17(11:11:53 +0000)

### Other

- [amx][gmap] Expose mdns information in gMap

## Release v0.4.2 - 2023-01-05(11:45:38 +0000)

### Other

- [amx][gmap] Expose mdns information in gMap

## Release v0.4.1 - 2022-11-30(14:06:08 +0000)

### Other

- [amx][gmap] mib-ip fails to load due to undefined symbol

## Release v0.4.0 - 2022-11-29(14:18:26 +0000)

### New

- [gMap][wanBlock] Development of a gMap wanblock module

## Release v0.3.0 - 2022-11-28(15:39:48 +0000)

### Other

- Add DHCPv4Client & DHCPv6Client in dhcp mib .odl

## Release v0.2.3 - 2021-09-13(13:39:40 +0000)

### Fixes

- [amxc] add ifdef around when_null_x, when_str_empty_x

## Release v0.2.2 - 2021-09-13(11:06:30 +0000)

### Fixes

- Use amxc_macros.h instead of local macros

## Release v0.2.1 - 2021-07-23(10:33:08 +0000)

### Changes

- gmap-mibs replace strcmp with gmap_ip_family_string2id

## Release v0.2.0 - 2021-07-22(14:26:30 +0000)

### New

- add ip mibs implementation to gmap-mibs-common

## Release v0.1.1 - 2021-05-14(08:55:58 +0000)

### Changes

- Remove fakeroot variable

## Release 0.1.0 - 2021-04-8(10:32:00 +0000)

### New

- Initial release
