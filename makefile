include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src/ip all
	$(MAKE) -C src/wan_access all
	$(MAKE) -C src/information all
	$(MAKE) -C src/useragent all
	$(MAKE) -C src/mdns-scanner all
	$(MAKE) -C src/priority all
	$(MAKE) -C src/stats all

clean:
	$(MAKE) -C src clean

install: all
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/gmap-server/mibs
	$(foreach odl,$(wildcard mibs/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/gmap-server/mibs/;)
	$(INSTALL) -D -p -m 0644 config/gmap_conf_location.odl $(DEST)/etc/amx/gmap-server/config/gmap_conf_location.odl
	$(INSTALL) -D -p -m 0644 config/gmap_conf_mdns-scanner.odl $(DEST)/etc/amx/gmap-server/config/gmap_conf_mdns-scanner.odl
	$(INSTALL) -D -p -m 0644 config/gmap_conf_priority.odl $(DEST)/etc/amx/gmap-server/config/gmap_conf_priority.odl
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_ip.so $(DEST)/usr/lib/amx/gmap-server/mib_ip.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_wan_access.so $(DEST)/usr/lib/amx/gmap-server/mib_wan_access.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_information.so $(DEST)/usr/lib/amx/gmap-server/mib_information.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_useragent.so $(DEST)/usr/lib/amx/gmap-server/mib_useragent.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_mdns-scanner.so $(DEST)/usr/lib/amx/gmap-server/mib_mdns-scanner.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_priority.so $(DEST)/usr/lib/amx/gmap-server/mib_priority.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_stats.so $(DEST)/usr/lib/amx/gmap-server/mib_stats.so

package: all
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/gmap-server/mibs
	$(INSTALL) -D -p -m 0644 mibs/*.odl $(PKGDIR)/etc/amx/gmap-server/mibs/
	$(INSTALL) -D -p -m 0644 config/gmap_conf_location.odl $(PKGDIR)/etc/amx/gmap-server/config/gmap_conf_location.odl
	$(INSTALL) -D -p -m 0644 config/gmap_conf_mdns-scanner.odl $(PKGDIR)/etc/amx/gmap-server/config/gmap_conf_mdns-scanner.odl
	$(INSTALL) -D -p -m 0644 config/gmap_conf_priority.odl $(PKGDIR)/etc/amx/gmap-server/config/gmap_conf_priority.odl
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_ip.so $(PKGDIR)/usr/lib/amx/gmap-server/mib_ip.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_wan_access.so $(PKGDIR)/usr/lib/amx/gmap-server/mib_wan_access.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_information.so $(PKGDIR)/usr/lib/amx/gmap-server/mib_information.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_useragent.so $(PKGDIR)/usr/lib/amx/gmap-server/mib_useragent.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_mdns-scanner.so $(PKGDIR)/usr/lib/amx/gmap-server/mib_mdns-scanner.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_priority.so $(PKGDIR)/usr/lib/amx/gmap-server/mib_priority.so
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/mib_stats.so $(PKGDIR)/usr/lib/amx/gmap-server/mib_stats.so
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package test